const { Pool } = require('pg');
const pool = new Pool();
const fs = require('fs');
const path = require('path');

let initialQuery = fs.readFileSync(path.join('src', 'sql', 'init.sql'), 'utf8');

let handleError = function (err) {
  switch (err.code) {
    case "23505": // duplicate of uniq key
      return Promise.reject({
        code: 409, // Conflict
      });
    case "23503":
      return Promise.reject({
        code: 400,
      });
    default:
      return Promise.reject({
        code: 500,
        message: err.message || "db error"
      });
  }
};

// the pool with emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err);
  process.exit(-1)
});

// promise - checkout a client
pool.connect()
  .then(client => {
    if (process.env.RUN_INIT_SQL)
      return client.query(initialQuery)
        .then(res => {
          client.release()
        })
        .catch(err => {
          console.log(err.stack);
          process.exit(-1);
        });
  })
  .catch(err => {
    console.error('DB connection error', err);
    process.exit(-1);
  });

exports.pool = pool;
exports.handleError = handleError;

exports.call = function (query) {
  return pool.query(query)
    .catch(handleError);
}

exports.tx = async callback => {
  const client = await pool.connect();
  try {
    await client.query('BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ');
    await callback(client);
    await client.query('COMMIT');
  } catch (e) {
    await client.query('ROLLBACK');
    throw e;
  } finally {
    client.release();
  }
}
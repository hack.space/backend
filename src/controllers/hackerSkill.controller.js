const { pool } = require('../pg-util');
const _ = require('underscore');
const { handleError } = require('../pg-util');

let db = {}; // for export;

function call(query) {
  return pool.query(query)
    .catch(handleError);
}

db.create = function (hackerId, skills) {
  if (!skills || skills.length === 0)
    return Promise.resolve();

  if (!Number(hackerId))
    return Promise.reject({ code: 400 });

  // add to skillHacker table
  let text = `INSERT INTO HackerSkills (hackerId, skillId, strength, countVerified)
  VALUES `;

  let i = 1;
  let values = [];
  let insertStatement = '';

  for (const skill of skills) {
    insertStatement += `('${hackerId}',$${i++},$${i++},$${i++}),\n`;
    values.push(
      skill.id,
      skill.strength || "0",
      skill.verified || "1"
    );
  }

  text += insertStatement.slice(0, -2); // removes ,\n from the end

  let query = {
    name: 'insert-hackerSkills',
    text,
    values
  };

  return call(query);
}

/**
 * Updates hacker's skills
 * @param {string} id of a hacker
 * @param {object[]} newSkills to set instead of current
 */
db.update = async function (hackerId, newSkills) {
  try {
    let { rows: curSkills } = await db.selectMany(hackerId);

    let curIds = curSkills.map(s => s.id);
    let newIds = newSkills.map(s => s.id);

    let addIds = _.difference(newIds, curIds);
    let removeIds = _.difference(curIds, newIds);

    let addSkillObjects = [];
    for (let skill of newSkills)
      if (addIds.includes(skill.id))
        addSkillObjects.push(skill);

    await db.delete(hackerId, removeIds);
    await db.create(hackerId, addSkillObjects);

    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
}

/**
 * Retrieves list of skills of a user by id.
 * @param {number} hackerId whos skills will be retrieved
 * @param {number} curUserId for defining had the user put a like
 */
db.selectMany = function (hackerId, curUserId = 0) {
  let text = `
  select s.id, s.tag, count(sv) as "verified",  
  bool_or(
          CASE WHEN sv.hacker_who = $1 THEN true ELSE false END
  ) as "liked"
  FROM HackerSkills hs
  LEFT JOIN Skills s on hs.skillId = s.id
  LEFT JOIN SkillVerifications sv on sv.hacker_whom = hs.hackerId and sv.skill_id = hs.skillId
  WHERE hs.hackerId = $2
  GROUP BY s.id;`

  let query = {
    name: 'select-hackerSkills',
    text,
    values: [curUserId, hackerId]
  };

  return call(query);
}

/**
 * Deletes rows from HackerSkills table
 * @param {string} hackerId id of a hacker
 * @param {string[]} skillIds ids of skills to remove
 */
db.delete = function (hackerId, skillIds) {
  if (!skillIds || skillIds.length === 0 || typeof hackerId !== 'number')
    return Promise.resolve();

  let text = `DELETE FROM HackerSkills
  WHERE hackerId = $1 and `;

  skillIds.forEach((el, i) => {
    text += `${i === 0 ? '(' : ' '}skillId = $${i + 2}${i === skillIds.length - 1 ? ')' : ' or'}`
  });

  let query = {
    name: 'delete-hackerSkills',
    text,
    values: _.flatten([hackerId, skillIds])
  }

  return call(query);
}

/**
 * Add verification row.
 * @param {number} who a hacker's id who put the like
 * @param {number} whom a hacker's id who will get the like
 * @param {number} skillId skill id
 * @param {callback} onDuplicate takes 3 args: who, whom, skillId
 */
db.addVerification = (who, whom, skillId, onDuplicate) => {
  let text = `INSERT INTO SkillVerifications (hacker_who, hacker_whom, skill_id)
  VALUES ($1, $2, $3)`;
  let query = {
    name: 'insert-skill-verification',
    text,
    values: [who, whom, skillId]
  }
  return pool.query(query)
    .catch(err => {
      if (err.code != 23505 || typeof onDuplicate !== 'function') // not a duplicate
        return handleError(err);
      return onDuplicate(who, whom, skillId);
    });
}

db.deleteVerification = function (who, whom, skillId) {
  let text = `DELETE FROM SkillVerifications 
  WHERE hacker_who = $1 and hacker_whom = $2 and skill_id = $3`;
  let query = {
    name: 'delete-skill-verification',
    text,
    values: [who, whom, skillId]
  }
  return call(query);
}

module.exports = db;
const { pool } = require('../pg-util');
const Promise = require('promise');
const { handleError } = require('../pg-util');

let db = {}; // is exported;

function call(query) {
  return pool.query(query)
    .catch(handleError);
}

const STATUSES = [
  "applied",
  "confirmed",
  "verified",
  "declined",
  "activated",
  "participated",
  "won"
]

/**
 * Fetches list of events according criterias.
 * @param {string} hackerId id of hacker
 * @param {string} status of participation. Any of: open, applied, confirmed, activated, participated, won - comma separated.
 * @param {number} limit how much items should be fetched
 * @param {number} offset how much items should be skipped
 * @returns {Promise} that will resolve matches
 */
db.selectMany = function (hackerId = 0, status = 'open', limit = 10, offset = 0) {

  /**
   * Creates sql-string for where clause
   * @param {string} statuses coma-separated
   * @return {boolean|string} false if not match or sql string
   */
  function statusInline(statuses) {
    if (!new RegExp(`^((${STATUSES.join('|')}),?)+$`).exec(statuses))
      return false;

    return statuses.split(',').map(s => `p.hackerstatus = '${s}'`).join(' or ');
  }

  /**
   * Creates second piece of sql query with where clause with filtering by statuses
   * @param {string} statuses for filtering coma separated
   */
  function generateQuery(statuses = "applied") {
    const whereClause = `WHERE p.hackerId = $1 AND (${statusInline(statuses)}) AND e.isdraft = false`;

    const text = `FROM Participations p
          LEFT JOIN Events e ON e.id = p.eventId
          LEFT JOIN Organizations o ON o.id = e.organizationId
          ${whereClause}`

    return text;
  }

  // Common begining
  let text = `SELECT 
  e.id, 
  e.title, 
  e.city, 
  e.dateStart, 
  e.dateEnd, 
  e.link,
  e.preview, 
  e.eventType, 
  e.isdraft, 
  p.hackerStatus, 
  o.id as "org_id", 
  o.title as "org_title",
  o.website as "org_website", 
  p.issearchable `

  // if status doesn't match any from STATUSES list OR Status is Open
  if (!statusInline(status))
    text += `
    FROM (
      SELECT e.id, e.title, e.city, e.dateStart, e.dateEnd, e.link,
        e.preview, e.eventType, e.organizationid, e.isDraft FROM Events e 
      EXCEPT (
        SELECT e.id, e.title, e.city, e.dateStart, e.dateEnd, e.link,
        e.preview, e.eventType, e.organizationid, e.isDraft from events e
        LEFT JOIN participations p on e.id = p.eventid
        WHERE p.hackerid = $1
      )
    ) e 
    LEFT JOIN organizations o on o.id = e.organizationId
    LEFT JOIN (
      SELECT 
        'open' as hackerstatus, 
        null as issearchable
      ) p on true
    WHERE e.dateStart > now() AND e.isDraft = false`;
  else
    text += ' ' + generateQuery(status);

  text += `
  ORDER BY e.dateStart
  LIMIT $2 
  OFFSET $3;`

  let query = {
    text,
    values: [hackerId, limit, offset]
  }

  return call(query)
    .then(({ rows }) => rows);
}

db.getAllEventsOfOrganization = (id) => {
  let text = `SELECT id, title as "name", dateStart, dateEnd,
        link, preview 
        FROM Events
        Where organizationId = $1 and isdraft = false`

  let query = {
    name: 'fetch-events-of-organizations',
    text,
    values: [id]
  };

  return call(query)
    .then(({ rows }) => rows);
}

db.getEventById = function (eventId) {
  let text = `
  select 
    e.*, 
    o.id as "org_id", 
    o.title as "org_title",
    o.website as "org_website" 
    from events e
  left join organizations o on o.id = e.organizationid
  WHERE e.id = $1 and e.isdraft = false`;

  let query = {
    name: 'select-event-by-id',
    text,
    values: [eventId]
  }

  return call(query)
    .then(res => res.rows[0]);
}

db.getEventSecrets = function (eventId) {
  let text = `select enteringword, location from events
  where id = $1`;

  let query = {
    name: 'select-event-secrets',
    text,
    values: [eventId]
  }

  return call(query)
    .then(({ rows }) => rows[0]);
}

module.exports = db;
const { pool } = require('../pg-util');
const Promise = require('promise');
const { handleError } = require('../pg-util');

let db = {};

const STATUSES = [
  'applied', // подал заявку
  'confirmed', // подтвержден организаторами
  'declined', // заявка отклонена организаторами
  'activated', // сделал чек-ин на хакатоне
  'participated', // ушел после активации на хакатоне
  'won' // выиграл хак
]

const VERIFICATION_STATUS = {
  unverified: 'unverified', // не подтвержден
  cancelled: 'cancelled', // отменил участие
  oneM: '1_m',  
  oneW: '1_w', 
  oneD: '1_d', 
  notSure: 'not_sure',
}

db.VERIFICATION_STATUS = VERIFICATION_STATUS;

function call(query) {
  return pool.query(query)
    .catch(handleError);
}

db.create = (hackerId, eventId, status, xp = 0, coins = 0) => {
  if (typeof parseInt(xp) !== 'number' || typeof parseInt(coins) !== 'number' ||
    typeof parseInt(hackerId) !== 'number' || typeof parseInt(eventId) !== 'number')
    return Promise.reject({ code: 400 });

  status = STATUSES.includes(status) ? status : 'applied';

  let text = `INSERT INTO Participations (hackerId, eventId, hackerStatus, xp, coins)
  VALUES ($1, $2, $3, $4, $5)`;

  let query = {
    name: 'insert-participation',
    text,
    values: [hackerId, eventId, status, xp, coins]
  }

  return call(query);
}

db.update = (hackerId, eventId, status = 'applied', xp = 0, coins = 0) => {
  if (typeof xp !== 'number' || typeof coins !== 'number')
    return Promise.reject({ code: 400 });

  if (!STATUSES.includes(status))
    return Promise.reject({ code: 500, message: 'incorrect hacker status: ' + status });

  let text = `UPDATE Participations
  SET (hackerstatus, xp, coins) = ($1, xp + $2, coins + $3)
  WHERE hackerId = $4 and eventId = $5`;

  let query = {
    name: 'update-participation',
    text,
    values: [status, xp, coins, hackerId, eventId]
  }

  return call(query);
}

db.getById = (hackerId, eventId) => {
  let whereStatement = '';
  let values = [];

  if (hackerId && !eventId) {
    whereStatement = `hackerId = $1`;
    values.push(hackerId);
  } else if (!hackerId && eventId) {
    whereStatement = `eventId = $1`;
    values.push(eventId);
  } else if (hackerId && eventId) {
    whereStatement = `hackerId = $1 and eventId = $2`;
    values.push(hackerId, eventId);
  }

  let text = `SELECT * FROM Participations
  WHERE ` + whereStatement;

  let query = {
    name: 'select-participation-by-id',
    text,
    values
  }

  return call(query)
    .then(({ rows }) => {
      if (hackerId && eventId) {
        if (!rows[0])
          return { hackerstatus: 'open' };
        return rows[0]
      }
      return rows;
    });
}

db.toggleSearchble = (hackerId, eventId) => {
  let text = `UPDATE Participations
  SET isSearchable = NOT isSearchable
  WHERE hackerId = $1 and eventId = $2
  RETURNING isSearchable`;

  let query = {
    name: 'update-hackers-field',
    text,
    values: [hackerId, eventId]
  }

  return call(query)
    .then(({ rows }) => rows[0]);
}

db.remove = (hackerId, eventId) => {
  const text = `DELETE from Participations
  WHERE eventId = $1 and hackerId = $2;`;
  const query = {
    name: 'delete-participation',
    text,
    values: [eventId, hackerId],
  }
  return call(query);
}

db.updateVerificationStatus = (hackerId, eventId, newStatus, reason) => {
  if (Object.values(VERIFICATION_STATUS).indexOf(newStatus) === -1)
    return Promise.reject({code: 400, message: `Verification status should be one of [${Object.values(VERIFICATION_STATUS).join(',')}]`});

  let text, values, i = 1;
  if (reason) {
    text = `UPDATE Participations SET (verification_status, leave_reason) = (?, ?)
    WHERE hackerId = ? and eventId = ?`;
    values = [newStatus, reason, hackerId, eventId];
  } else {
    text = `UPDATE Participations SET verification_status = ?
    WHERE hackerId = ? and eventId = ?`;
  }
  const query = {
    text: text.replace(/\?/g, () => `$${i++}`),
    values,
  }
  return call(query);
}

db.setTeamId = (eventId, teamId, hackerId, client) => {
  return client.query({
    text: `
      update participations
      set team_id = $1
      where eventid = $2 and hackerid = $3;
    `,
    values: [teamId, eventId, hackerId]
  });
};


module.exports = db;
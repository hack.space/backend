const { pool } = require('../pg-util');
const Promise = require('promise');
const { handleError } = require("../pg-util"); 

function getList() {
  let query = `SELECT * FROM Skills`;

  return pool.query(query)
    .catch(handleError);
}

exports.getList = () => {
  return getList()
    .then(res => res.rows);
};
const
  { pool } = require('../pg-util'),
  Promise = require('promise'),
  { generatePasswordHash } = require('../plugins/authN'),
  { handleError } = require('../pg-util'),
  _ = require('lodash');

let db = {}; // is exported

const tableProperties = Object.freeze([
  'username',
  'city',
  'tgProfileLink',
  'email',
  'email_is_verified',
  'contactPhone',
  'contact_phone_is_verified',
  'bio',
  'pic',
  'pwd',
  'salt',
  'tgId',
  'addr',
  'slackId',
  'slackDeepLink',
]);

/**
 * Generates String for parametrs in SQL:
 * @example
 * input: count = 3, startIndex = 3
 * output: ",$3,$4,$5" 
 */
function numLine(count, startIndex = 1) {
  let tail = startIndex;
  let str = '';
  while ((startIndex - tail) !== count) { str += `,$${startIndex++}` }
  return tail === 1 ? str.slice(1) : str; // removes a coma from front
}

function call(query) {
  return pool.query(query)
    .catch(handleError);
}

/**
 * Creates new row in Hackers table in DB.
 * @param {object} data which will be inserted
 * @returns {Promise} postgres meta
 */
db.insert = function (data) {
  let keys = _.intersection(tableProperties, Object.keys(data));
  let values = keys.map(key => data[key])

  let text = `INSERT INTO Hackers (${keys})
    VALUES (${numLine(keys.length)})
    RETURNING id;`;

  let query = { text, values };
  return call(query);
}

/**
 * Updates row in Hackers table by id.
 * @param {string} id of hacker to update
 * @param {object} data object with new data
 */
db.update = function (id, data) {
  let keys = _.intersection(tableProperties, Object.keys(data))
  if (keys.length === 0) return
  let values = keys.map(key => data[key])
  values.push(id)

  let text = `UPDATE Hackers
  SET (${keys}) = (${numLine(keys.length)})
  WHERE id = $${keys.length + 1}`

  // removes round brackets from SET statement if there is one argument
  if (keys.length === 1)
    text = text.replace(/(\(|\))/g, '')

  let query = { text, values }
  return call(query)
}

/**
 * Fetches a hacker by id.
 * @param {string} id of a hacker
 * @returns {Promise}
 */
db.selectOne = function (id) {
  let text = `SELECT h.id, h.pic, h.username, h.bio, h.email,
    h.email_is_verified, h.contact_phone_is_verified,
    h.tgProfileLink, h.tgId, h.contactPhone, h.city, 
    h.slackid, h.slackdeeplink,
    coalesce(sum(p.xp),0) as xp, 
    sum(case when p.hackerStatus = 'won' then 1 else 0 end) as "hackWin",
    sum(case when p.hackerStatus = 'won' or p.hackerStatus = 'activated' or p.hackerStatus = 'participated' 
      then 1 else 0 end) as "hackTotal",
    coalesce(sum(p.coins),0) as coins 
    FROM Hackers h
    LEFT JOIN Participations p ON p.hackerId = h.id
    LEFT JOIN Events e ON e.id = p.eventId
    WHERE h.id = $1
    GROUP BY h.id`;

  let query = { 
    name: 'fetch-hacker-by-id',
    text,
    values: [id]
  };

  return call(query);
}

db.selectOneWhereSlackId = function (slackId) {
  let text = `SELECT * FROM Hackers
  WHERE slackId = $1`;

  let query = {
    name: 'select-hacker-by-slack-id',
    text,
    values: [slackId]
  }

  return call(query);
}

db.selectOneWhereTgId = function (tgId) {
  let text = `SELECT * FROM Hackers
  WHERE tgId = $1`;

  let query = {
    name: 'select-hacker-by-tgid',
    text,
    values: [tgId]
  }

  return call(query);
}

db.selectOneWhereEmail = function (email, includeNotVerified = false) {
  let text = `SELECT * FROM Hackers
  WHERE email = $1` + (includeNotVerified ? '' : ` and email_is_verified = true`);

  let query = {
    text,
    values: [email]
  }

  return call(query);
}

/**
 * Fetches list of hackers objects.
 * @param {string} sortBy name of param to sort by
 * @param {number} page number, started with 0
 * @param {number} count of items which need to be fetched
 * @returns {Promise}
 */
db.selectMany = function (sortBy, page = 0, count = 100, eventId) {
  sortBy = sortBy === 'wins' ? 'wins' : 'xp'; // prevents sql-injection
  if (typeof page !== "number" || typeof count !== "number")
    return Promise.reject({ code: 400 });
  let values = [count, count * page];
  if (eventId) values.push(eventId);

  let text = `
  SELECT paged.*, ${eventId ? 'isSearchable, hackerStatus,' : ''} (${page * count} + row_number() OVER()) as position FROM (
    SELECT h.*,
    COALESCE(json_agg(s.tag) FILTER (WHERE s.tag IS NOT NULL), '[]') as "skills"
    FROM (
      SELECT 
      -- TODO: Remove h.tgProfileLink when bot will be updated
      h.id, h.pic, h.username, h.tgProfileLink,
      coalesce(sum(p.xp),0) as "xp",
      sum(case when p.hackerStatus = 'won' then 1 else 0 end) as "wins" 
      FROM Hackers h
      LEFT JOIN Participations p ON p.hackerId = h.id
      GROUP BY h.id
    ) h
    LEFT JOIN HackerSkills hs on hs.hackerId = h.id
    LEFT JOIN Skills s ON hs.skillId = s.id
    GROUP BY h.id, h.pic, h.username, h.tgProfileLink, h.xp, h.wins
    ORDER BY ${sortBy} DESC
    LIMIT $1
    OFFSET $2) paged
    `
    + (eventId
      ? `LEFT JOIN Participations p ON p.hackerId = id
       WHERE p.eventId = $3`
      : '')

  let query = {
    name: 'fetch-hackers',
    text,
    values
  };

  return call(query);
}

db.remove = function (id) {
  let text = `delete from hackers where id = $1`
  let query = {
    name: 'delete-hacker',
    text,
    values: [id]
  }
  return call(query)
}

module.exports = db;
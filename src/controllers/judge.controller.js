const { call } = require('../pg-util');

let db = {}

const JUDGE_STATUS = {
  created: 0,
  invited: 1,
  activated: 2,
  completed: 3
}

const JUDGE_TYPES = {
  jury: 0,
  participant: 1
}

db.insert = (obj, status = 'created') => {
  if (!JUDGE_STATUS[obj.status])
    status = 'created';

  let text = `INSERT INTO Judges
  (name,email,token,event_id,status,type,profile_id)
  VALUES ($1,$2,$3,$4,$5,$6,$7)
  RETURNING id;`

  let query = {
    text,
    values: [obj.name, obj.email, obj.token, obj.eventId, status, JUDGE_TYPES.participant, obj.profileId]
  }

  return call(query)
}

db.updateToken = (id, token) => {
  let text = `UPDATE Judges
  SET token = $1
  WHERE id = $2`
  let query = {
    text,
    values: [token, id]
  }
  return call(query)
}

db.get = (hackerId, eventId) => {
  let text = `SELECT * FROM Judges
  WHERE profile_id = $1 and event_id = $2`
  let query = {
    text,
    values: [hackerId, eventId]
  }
  return call(query)
    .then(({rows}) => rows[0]);
}

module.exports = db;
const { promisify } = require('util');
const redis = require("redis");
const hackerDb = require('../../controllers/hacker.controller')
const {
  REDIS_HOST,
  REDIS_PORT,
  REDIS_DB,
  EMAIL_CHECK_EXPIRATION_TIME: EXPIRATION_TIME
} = require('../../config');
const client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT,
  db: REDIS_DB
});
const getAllAsync = promisify(client.hgetall).bind(client);
const setAsync = promisify(client.hmset).bind(client);
const delAsync = promisify(client.del).bind(client);
const scanAsync = promisify(client.scan).bind(client);

client.on("error", function (err) {
  console.error("Redis: " + err);
  process.exit(-1);
});

async function set(email, { code, id, checked = 0, expiresin, onDel = '' }) {
  if (typeof email !== 'string') return;
  return setAsync(email.toLowerCase(), [
    'code', code,
    'id', id,
    'expiresin', expiresin || Date.now() + EXPIRATION_TIME,
    'checked', checked,
    'onDel', onDel
  ])
}

async function get(email, counts = true) {
  if (typeof email !== 'string') return;
  let row = await getAllAsync(email.toLowerCase())
  if (!row) return null;
  if (counts) {
    row.checked++
    await set(email, row)
  }
  return row
}

function del(email) {
  return delAsync(email)
}

function afterDelete(data) {
  // TODO: bad logical injection (!DRY)
  switch (data.onDel) {
    case 'UPDATE':
      return hackerDb.update(data.id, { email: null })
    case 'DELETE': 
      return hackerDb.remove(data.id)
    default:
      // do nothing;
      break;
  }
}

async function removeIfExpired(email) {
  try {
    let data = await get(email, false)
    if (!data || data.expiresin > Date.now()) return;
    await delAsync(email)
    await afterDelete(data)
  } catch (err) {
    // console.error('Confirmer:RemoveIfExpired: ', err)
  }
}

async function observeOutdatedEmails() {
  var cursor = 0
  do {
    try {
      var [cursor, emails] = await scanAsync(cursor)
      if (emails && emails.length > 0)
        console.debug('emails ', emails)
      for (let email of emails)
        setImmediate(removeIfExpired, email) // not-blocking
    } catch (err) {
      console.error('Confirmer:observeOutdatedEmails: ', err)
    }
  } while (cursor != 0)
}

if (EXPIRATION_TIME) {
  setInterval(observeOutdatedEmails, EXPIRATION_TIME / 10);
} else {
  console.warn('Email checker worker is dissabled');
}

module.exports = { get, set, del }
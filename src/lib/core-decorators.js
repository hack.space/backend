"use strict;"

/**
 * Adds try-catch block around a target function.
 * @param {function} target 
 */
function tryCatchDecorator(target) {
  return async function () {
    try {
      return await target.apply(this, arguments)
    } catch (err) {
      return Promise.reject(err)
    }
  }
}

/**
 * Add general decorators to functions of a unit.
 * @param {object} unit object with functions
 * @returns {object} decorated unit
 */
exports.decore = (unit, options = {}) => {
  if (typeof unit !== 'object') return unit;

  const {
    tryCatch = true
  } = options;

  for (const key in unit) {
    if (typeof unit[key] === 'function') {
      if (tryCatch)
        unit[key] = tryCatchDecorator(unit[key])
    }
  }
  return unit;
}
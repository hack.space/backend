const { validate } = require('jsonschema');

/**
 * Removes unproper fields (extra) in object which are not
 * in schema. 
 * @param {object} instance 
 * @param {object} schema 
 * 
 * Known limitation: works only keys of first level of an object.
 */
function removeExtraFields(instance, schema) {
  if (Array.isArray(instance))
    return;
  let keys = Object.keys(schema.properties);
  for (let key in instance) {
    if (keys.indexOf(key) === -1)
      delete instance[key]
  }
}

/**
 * Promose for classic validate function.
 */
exports.validate = function (instance, schema, code = 400) {
  removeExtraFields(instance, schema)
  let validationRes = validate(instance, schema);
  if (!validationRes.valid)
    return Promise.reject({
      code,
      message: validationRes.errors
    });
  else
    return Promise.resolve();
}
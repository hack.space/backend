const fs = require('fs');
const { join } = require('path');

module.exports = fs.readFileSync(join(__dirname, 'email-template.html'), 'utf8')
  .replace('#afterwords', 'If you did not create a Hackathons.Space account using this address, simply ignore this message.')
  .replace('#company_info', 'Hackspace Inc., Moscow')
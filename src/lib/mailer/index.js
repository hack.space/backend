const Promise = require('promise');
const nodemailer = require('nodemailer');
const { EMAIL_USER, EMAIL_PWD } = require('../../config')
const BASE_CONFIRM_URL = require('../../config').url + `/mail-confirmation/`

let htmlTemplate = require('./build-template')
let smtpTransport;

try {
  smtpTransport = nodemailer.createTransport({
    host: 'smtp.yandex.ru',
    port: 465,
    secure: true, // true for 465, false for other ports 587
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PWD
    }
  });
} catch (err) {
  console.error('Init mailer error:', err)
  process.exit(-1);
}

let unit = {}

/**
 * Sends invitation letter
 * @param {string[]} receivers list of receivers
 * @param {object} details - for the letter's customization
 * @param {string} details.name - name of a user
 * @param {string} details.code - confirmation code
 * @returns {promise} that resolves delivery info
 */
unit.sendMail = (receivers, details) => {
  receivers = typeof receivers === 'string' ? receivers : receivers.join(',');
  let mailOptions = {
    from: `"Hackspace Team" ${EMAIL_USER}`,
    to: receivers, // list of receivers comma separated
    subject: 'Email verification ',
    html: customizeLetter(details) // html body
  };

  return new Promise((resolve, reject) => {
    smtpTransport.sendMail(mailOptions, (err, info) => {
      if (err) {
        console.error('Sending mail error', err);
        return reject(err)
      }

      console.debug('Message sent: %s', JSON.stringify(info));
      resolve(info)
    })
  })
}

unit.sendRowEmail = (receivers, subject, text) => {
  receivers = typeof receivers === 'string' ? receivers : receivers.join(',');
  const mailOptions = {
    from: `"Hackspace Team" ${EMAIL_USER}`,
    to: receivers, // list of receivers comma separated
    subject,
    text
  }
  return new Promise((resolve, reject) => {
    smtpTransport.sendMail(mailOptions, (err, info) => {
      if (err) {
        console.error('Error', err);
        return reject(err)
      }

      console.debug('Message sent: %s', JSON.stringify(info));
      resolve(info)
    })
  })
}

function customizeLetter({ name, code }) {
  return htmlTemplate
    .replace('#message', `Hi, ${name}! Your code is ${code}.`)
}

module.exports = unit;
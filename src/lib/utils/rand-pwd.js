// generates 8 random chars passwword
module.exports = () => {
    return Math.random().toString(36).slice(-8);
}
const { url, RESTORE_PASSWORD_ROOT, } = require('../../config');

const makeVotingLink = (token) => `${url}/voting/welcome/${token}`;
const makeRestorePwdLink = (code, isNew = false) => url + RESTORE_PASSWORD_ROOT + `?code=${code}&isNew=${isNew}`;

module.exports = {
	makeVotingLink,
	makeRestorePwdLink,
}
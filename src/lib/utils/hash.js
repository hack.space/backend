const crypto = require('crypto');

const createHash = (str) => crypto.createHash('sha256').update(str).digest();
const compareHash = (str, hash) => hash === createHash(str);

module.exports = { createHash, compareHash };
const { getStatusText } = require('http-status-codes');

const errorHandler = function (err, res) {
  let code;
  if (err.code >= 400 && err.code < 500)
    code = err.code;
  else code = 500;

  let message = err.message ? err.message : getStatusText(code);

  if (code === 500)
    console.error(err);

  if (code === 429 && err.retryAfter)
    res.setHeader('Retry-After', err.retryAfter)

  return res.status(code).send({ message });
}

class HttpError extends Error {
  constructor(code, message) {
    super();
    this.code = code;
    this.message = message;
  }
}

class NotFoundError extends HttpError {
  constructor(message) {
    super(404, message);
  }
}

class BadRequestError extends HttpError {
  constructor(message) {
    super(400, message);
  }
}

class NotAuthorizedError extends HttpError {
  constructor(message) {
    super(401, message);
  }
}

module.exports = errorHandler;

module.exports.HttpError = HttpError;
module.exports.NotFoundError = NotFoundError;
module.exports.BadRequestError = BadRequestError;
module.exports.NotAuthorizedError = NotAuthorizedError;
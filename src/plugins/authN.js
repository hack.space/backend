const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const { SECRET } = require('../config')
let unit = {};

if (!SECRET) {
  console.error('Environment variable SECRET is required.');
  process.exit(-1);
}

/**
 * Generates jwt token.
 * @param {obj} data - payload
 * @param {string} expiresIn
 */
unit.makeToken = (data, expiresIn = '21d', secret = SECRET) => {
  return jwt.sign(data, secret, { expiresIn });
}

let decodeToken = (token, secret = SECRET) => {
  return jwt.verify(token, secret);
}

/**
 * Generates salt and pwdHash from it.
 * @param {string} pwd user's password
 * @param {string?} mySalt if not set, will be generated
 * @returns {object} containing generated `salt` and `pwdHash`
 */
unit.generatePasswordHash = (pwd, mySalt = '') => {
  let salt = mySalt || crypto.randomBytes(128)
    .toString('hex')
    .slice(0, 256);

  let pwdHash = crypto.createHmac('sha256', salt)
    .update(pwd)
    .digest('hex');

  return { salt, pwdHash };
}

/**
 * Raises 401 error if not authorized, otherwise
 * set _acl (decoded jwt) in req.
 */
function authenticate(req, res, next) {
  try {
    let token = req.headers['authorization'];
    let data = decodeToken(token);

    if (data.email_verification)
      throw new Error('unverified');

    req._acl = data;

    next();
  } catch (err) {
    res.status(401).send();
  }
}

/**
 * Returnes decodes jwt or null.
 */
function authenticateNotStrict(req, res, next) {
  try {
    const token = req.headers['authorization'];
    const data = decodeToken(token)
    req._acl = data;
  } catch (err) {
    req._acl = null; // explicitly set null
  } finally {
    next();
  }
}

unit.middleware = (notStrict = false) => {
  if (notStrict)
    return authenticateNotStrict;

  return authenticate;
}

module.exports = unit;
const ENV = process.env.NODE_ENV;
const HOST = process.env.API_HOST;
const HTTP = process.env.API_HTTP === 'http' ? 'http://' : 'https://';
// common settings
let config = {
  url: HTTP + HOST,
  REDIS_HOST: process.env.REDIS_HOST || '127.0.0.1',
  REDIS_PORT: process.env.REDIS_PORT || 6379,
  REDIS_DB: process.env.REDIS_DB || 0,
  EMAIL_USER: process.env.EMAIL_USER,
  EMAIL_PWD: process.env.EMAIL_PWD,
  SECRET: process.env.SECRET,
  ADMIN_SECRET: process.env.ADMIN_SECRET,
  CHECKIN_DIST: process.env.CHECKIN_DIST || 0.1,
  RESTORE_PASSWORD_ROOT: process.env.RESTORE_PASSWORD_ROOT || '/restore',

  SLACK_CLIENT_ID: process.env.SLACK_CLIENT_ID,
  SLACK_CLIENT_SECRET: process.env.SLACK_CLIENT_SECRET,
  SLACK_REDIRECT_URI: process.env.SLACK_REDIRECT_URI || '',
  SLACK_TEAM_ID: process.env.SLACK_TEAM_ID || '',
  FRONT_AUTH_REDIRECT_URI: process.env.FRONT_AUTH_REDIRECT_URI || '',

  T_HACK_ID: process.env.T_HACK_ID || 1,
};

if (ENV === 'prod' || ENV === 'production') {
  Object.assign(config, {
    EMAIL_CHECK_RESEND_TIME_LIMIT: 60 * 1000, // 60sc.
    EMAIL_CHECK_EXPIRATION_TIME: 15 * 60 * 1000, // 15 mins in ms.
  });

  if (!HOST) {
    config.url = 'https://hackspace.pro'
  }

  console.debug = () => {};
  console.log("Running in Production environment")
}
else { // dev by default
  Object.assign(config, {
    EMAIL_CHECK_RESEND_TIME_LIMIT: 30 * 1000, // 30sc.
    EMAIL_CHECK_EXPIRATION_TIME: 2 * 60 * 1000, // 2 min in ms.
  });

  if (!HOST) {
    config.url = 'https://hackspace.pw';
  }

  console.warn("Running in Development environment")
}

module.exports = Object.freeze(config);

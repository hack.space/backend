const express = require('express');
const router = express.Router();
const esb = require('../ESBs/events.esb');
const authN = require('../plugins/authN');
const errorHandler = require('../plugins/errorHandler');

router.get('/', authN.middleware(true), function (req, res) {
  const hackerId = req.query.hacker_id || req._acl && req._acl.id;

  esb.getList(hackerId, req.query)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

router.get('/:eventId', authN.middleware(true), function (req, res) {
  const hackerId = req.query.hacker_id || req._acl && req._acl.id;

  esb.getById(req.params.eventId, hackerId)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

// post new user's event
router.post('/:eventId/apply', authN.middleware(true), function (req, res) {
  const hackerId = req._acl && req._acl.id;
  const body = req.body;
  esb.apply(hackerId, req.params.eventId, body)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

router.post('/:eventId/toggle_is_searchable', authN.middleware(), function (req, res) {
  const hackerId = req._acl.id;
  esb.toggleIsSearchable(hackerId, req.params.eventId)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

router.post('/:eventId/activate', authN.middleware(), function(req, res) {
  const hackerId = req._acl.id;
  esb.activate(hackerId, req.params.eventId, req.body)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

router.post('/:eventId/finish', authN.middleware(), function(req, res) {
  const hackerId = req._acl.id;
  esb.finish(hackerId, req.params.eventId)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

router.post('/:eventId/leave', authN.middleware(), function(req, res) {
  const hackerId = req._acl.id;
  esb.leave(hackerId, req.params.eventId, req.body)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res))
});

module.exports = router;
const express = require('express');
const router = express.Router();
const esb = require('../ESBs/hackers');
const authN = require('../plugins/authN');
const errorHandler = require('../plugins/errorHandler');

router.get('/', authN.middleware(), function (req, res) {
  esb.getList(req.query)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.get('/:id', authN.middleware(), function (req, res) {
  esb.getById(req.params.id, req._acl.id)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

// Is used for signup. 
router.post('/', function (req, res) {
  esb.create(req.body, req.query)
    .then(result => { // contains jwt in token field
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.put('/me', authN.middleware(), function (req, res) {
  esb.update(req._acl.id, req.body)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.post('/:hackerId/:skillId/like', authN.middleware(), function (req, res) {
  esb.verifySkill(req._acl.id, req.params.hackerId, req.params.skillId)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.post('/email-verification', function (req, res) {
  esb.confirmEmail(req.body.email, req.body.code)
    .then(result => {
      res.send(result)
    })
    .catch(err => errorHandler(err, res))
});

router.post('/resend-confirm-code', function (req, res) {
  esb.resendConfirmCode(req.body.email)
    .then(result => {
      res.send(result)
    })
    .catch(err => errorHandler(err, res))
})

module.exports = router;
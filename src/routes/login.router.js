const express = require('express');
const router = express.Router();
const loginESB = require('../ESBs/login.esb');
const errorHandler = require('../plugins/errorHandler');
const axios = require('axios');
const config = require('../config');
const hackerDB = require('../controllers/hacker.controller');
const { makeToken } = require('../plugins/authN');
const qs = require('querystring');
const eventESB = require('../ESBs/events.esb');
const { NotFoundError } = require('../plugins/errorHandler');
const participationDB = require('../controllers/participation.controller')

const slackClientId = config.SLACK_CLIENT_ID;
const slackSecret = config.SLACK_CLIENT_SECRET;
const slackRedirect = config.SLACK_REDIRECT_URI;
const slackTeamId = config.SLACK_TEAM_ID;
const frontAuthRedirectUri = config.FRONT_AUTH_REDIRECT_URI;
const slackApi = axios.create({
  baseURL: 'https://slack.com/api'
});
const tHackId = config.T_HACK_ID;

const slackRequest = (method, params) => {
  return slackApi.get(method, { params });
}

router.get('/slack/button', function (req, res) {
  const params = {
    user_scope: 'identity.basic,identity.avatar',
    client_id: slackClientId,
    redirect_uri: slackRedirect,
    team: slackTeamId,
  };
  const queryParams = qs.encode(params);
  res.send(`
    <a href="https://slack.com/oauth/v2/authorize?${queryParams}">
      <img 
        alt=""Sign in with Slack"" 
        height="40" 
        width="172" 
        src="https://platform.slack-edge.com/img/sign_in_with_slack.png" 
        srcset="https://platform.slack-edge.com/img/sign_in_with_slack.png 1x, https://platform.slack-edge.com/img/sign_in_with_slack@2x.png 2x" 
      />
    </a>
  `);
});

router.get('/slack/finish', async (req, res) => {
  try {
    const { code, state } = req.query;
    const { data } = await slackRequest('oauth.v2.access', {
      client_id: slackClientId,
      client_secret: slackSecret,
      redirect_uri: slackRedirect,
      code,
      state,
    });
    if (!data.ok) {
      console.error(data);
      const result = qs.encode({ result: 'ERROR', error: 'oauth.v2.access' });
      return res.redirect(frontAuthRedirectUri + '/?' + result);
    }

    const token = data.authed_user.access_token;
    const slackUserId = data.authed_user.id;

    const { data: { ok, user, team } } = await slackRequest('users.identity', { token });
    if (!ok) {
      const result = qs.encode({ result: 'ERROR', error: 'user.identity' });
      return res.redirect(frontAuthRedirectUri + '/?' + result);
    }

    if (slackTeamId && team.id !== slackTeamId) {
      const result = qs.encode({ result: 'ERROR', error: 'invalid_team' });
      return res.redirect(frontAuthRedirectUri + '/?' + result);
    }

    const { rows } = await hackerDB.selectOneWhereSlackId(slackUserId);
    if (rows && rows.length > 0) {
      const appUser = rows[0];
      const jwt = makeToken({ id: appUser.id, role: 'hacker' });
      const application = await participationDB.getById(appUser.id, tHackId);
      if (!application || application.hackerstatus === 'open') {
        await eventESB.apply(appUser.id, tHackId);
      }

      return res.redirect(frontAuthRedirectUri + '/?jwt=' + jwt);
    }

    const pic = user.image_192;
    const username = user.name;
    const slackDeepLink = `slack://user?team=${team.id}&id=${user.id}`;

    var id = (await hackerDB.insert({
      pwd: '',
      skills: [],
      slackId: slackUserId,
      username,
      pic,
      slackDeepLink,
    })).rows[0].id;
    await eventESB.apply(id, tHackId);
    const jwt = makeToken({ id, role: 'hacker' });
    return res.redirect(frontAuthRedirectUri + '/?jwt=' + jwt);
  } catch (err) {
    console.error('ERROR IN SLACK OUATH', JSON.stringify(err));
    const result = qs.encode({ result: 'ERROR', error: 'unknown' });
    return res.redirect(frontAuthRedirectUri + '/?' + result);
  }
});

router.post('/', function (req, res) {
  loginESB.basicLogin(req.body, req.query)
    .then(result => { // contains jwt in token field
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.post('/telegram', async function (req, res) {
  loginESB.telegramLogin(req.body, req.query)
    .then(result => { // contains jwt in token field
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.post('/forgot', async (req, res) => {
  const { email } = req.body;
  try {
    const result = await loginESB.requestPasswordRestore(email);
    res.send(result);
  } catch (err) {
    errorHandler(err, res);
  }
});

router.post('/restore', async (req, res) => {
  const { code, password } = req.body;
  try {
    const result = await loginESB.setNewPassword(code, password);
    res.send(result);
  } catch (err) {
    errorHandler(err, res);
  }
});

module.exports = router;
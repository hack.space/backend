const express = require('express');
const router = express.Router();
const db = require('../controllers/skill.controller');
const authN = require('../plugins/authN');
const errorHandler = require('../plugins/errorHandler');

router.get('/', function (req, res) {
  db.getList()
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

module.exports = router;
const express = require('express');
const router = express.Router();
const errorHandler = require('../plugins/errorHandler');
const config = require('../config');
const {call, tx} = require('../pg-util');
const authN = require('../plugins/authN');
const participationDB = require('../controllers/participation.controller');

function getCheckpoints () {
  return [
    {
      id: 1,
      name: "Загрузка идеи проекта.",
      status: "done",
      status_lang: "Выполнено",
      date: {
        week: "Чт",
        time: "19/11"
      }
    },
    {
      id: 2,
      name: "Проверка ментора #1",
      status: "",
      status_lang: "Запланировано на 20:00 Мск",
      date: {
        week: "Пт",
        time: "20/11"
      }
    },
    {
      id: 3,
      name: "Проверка ментора #2",
      status: "",
      status_lang: "Запланировано на 10:00 Мск",
      date: {
        week: "Сб",
        time: "21/11"
      }
    },
    {
      id: 4,
      name: "Проверка ментора #3",
      status: "",
      status_lang: "Запланировано на 19:00 Мск",
      date: {
        week: "Сб",
        time: "21/11"
      }
    },
    {
      id: 5,
      name: "Проверка ментора #4",
      status: "",
      status_lang: "Запланировано на 10:00 Мск",
      date: {
        week: "Вс",
        time: "22/11"
      }
    },
    {
      id: 6,
      name: "DEMO - Зрительское голосование",
      status: "",
      status_lang: "Запланировано на 15:00 Мск",
      date: {
        week: "Вс",
        time: "22/11"
      }
    },
    {
      id: 7,
      name: "Запуск тестирования на сотрудниках",
      status: "",
      status_lang: "Запланировано на 11:00 Мск",
      date: {
        week: "Пн",
        time: "23/11"
      }
    },
    {
      id: 8,
      name: "Финальные презентации",
      status: "",
      status_lang: "",
      date: {
        week: "TBA",
        time: ""
      }
    }
  ];
}

async function getTeams(eventId, teamId) {
  return call({
    name: 'get-teams',
    text: `
      select 
        t.id as "teamId",
        t.name as "teamName",
        t.twit as "teamTwit",
        t.max_members as "teamLimit",
        t.teamlead_id as "teamLeadId",
      
        h.id as "hackerId",
        h.username as "hackerName",
        h.pic as "hackerImage",
        h.skills as "hackerSkills",
        h.bio as "hackerBio",
        h.slackdeeplink as "hackerLink"
      
      from participations p
        left join (
        select h.*, hs.skills from (select h.id, json_agg(s.tag) as "skills"
          from hackers h left join hackerskills hs on h.id = hs.hackerid
          left join skills s on hs.skillid = s.id
          group by h.id) hs left join hackers h on hs.id = h.id
        ) h
          on h.id = p.hackerid
        left join teams t
          on t.id = p.team_id
      where p.eventid = $1 
          ${teamId ? ' and t.id = $2' : ' and (t.is_searchable is null or t.is_searchable = true)'}
      ;
    `,
    values: teamId ? [eventId, teamId] : [eventId],
  }).then(({rows}) => {
    const teams = {};
    const users = [];

    for (let {teamId, teamName, teamTwit, teamLimit, teamLeadId, hackerId, hackerName, hackerImage, hackerSkills, hackerBio, hackerLink} of rows) {
      const teammate = {
        id: hackerId,
        name: hackerName,
        description: hackerBio,
        skills: hackerSkills,
        image: hackerImage,
        link: hackerLink,
      }
      if (teamId) {
        if (teams[teamId]) {
          teams[teamId].teammates.push(teammate);
        } else {
          const team = {
            id: teamId,
            name: teamName,
            description: teamTwit,
            limit: teamLimit,
            leadId: teamLeadId,
            teammates: [teammate]
          }
          teams[team.id] = team;
        }
      } else {
      users.push(teammate);
      }
    }

    return {
      teams: Object.values(teams).sort((t1, t2) => t1.teammates.length - t2.teammates.length), 
      users: users.filter(u => u.description || (u.skills && u.skills.length && u.skills.filter(el => !!el).length > 0)),
    };
  });
}

const mapTeamEnttityToDto = (entity) => {
  return {
    id: entity.id,
    find: entity.is_searchable,
    name: entity.name,
    idea: entity.idea,
    repo: entity.repo,
    description: entity.twit,
    limit: entity.max_members,
    leadId: entity.teamlead_id,
    slides: entity.slides,
  }
}

async function findTeamById(id) {
  return call({
    text: 'select * from teams where id = $1 and event_id = $2',
    values: [id, config.T_HACK_ID],
  }).then(({rows}) => rows[0] && mapTeamEnttityToDto(rows[0]));
}

async function findTeamByName(name) {
  return call({
    text: 'select * from teams where name = $1 and event_id = $2',
    values: [name, config.T_HACK_ID],
  }).then(({rows}) => rows[0] && mapTeamEnttityToDto(rows[0]));
}

async function createTeam({find = true, name, idea, description, repo, leadId}, client) {
  if (!name || typeof name !== 'string' || !name.trim()) {
    return {error: 'Название команды обязательно.'};
  }

  name = name.trim();

  if (await findTeamByName(name)) {
    return {error: 'К сожалению данное имя уже занято.'}
  }

  return client.query({
    text: `
      insert into teams (name, idea, twit, repo, teamlead_id, is_searchable, event_id)
      values ($1, $2, $3, $4, $5, $6, $7)
      returning id;
    `,
    values: [name, idea, description, repo, leadId, find, config.T_HACK_ID],
  });
}

/**
 * Generates String for parametrs in SQL:
 * @example
 * input: count = 3, startIndex = 3
 * output: ",$3,$4,$5" 
 */
function numLine(count, startIndex = 1) {
  let tail = startIndex;
  let str = '';
  while ((startIndex - tail) !== count) { str += `,$${startIndex++}` }
  return tail === 1 ? str.slice(1) : str; // removes a coma from front
}

async function updateTeamLeadId(teamId, leadId, client) {
  return client.query({
    text: `update teams set teamlead_id = $1 where id = $2`,
    values: [leadId, teamId],
  })
}

async function updateTeam(teamId, {find, name, idea, description, repo}) {
  const keys = [];
  const values = [];

  if (name && typeof name === 'string' && !!name.trim()) {
    name = name.trim();
    if (await findTeamByName(name)) {
      return {error: 'Такое название уже занято кем-то другим.'}
    }
    keys.push('name');
    values.push(name);
  }
  if (idea && typeof idea === 'string' && !!idea.trim()) {
    keys.push('idea');
    values.push(idea);
  }
  if (description && typeof description === 'string' && !!description.trim()) {
    keys.push('twit');
    values.push(description);
  }
  if (repo && typeof repo === 'string' && !!repo.trim()) {
    keys.push('repo');
    values.push(repo);
  }
  if (find !== null && typeof find === 'boolean') {
    keys.push('is_searchable');
    values.push(find);
  }

  if (keys.length === 0) {
    return {error: 'Укажите параметры.'}
  }
  let text = `
    update teams set (${keys}) = (${numLine(keys.length)})
    where id = $${keys.length+1}
  `;
  if (keys.length === 1) {
    text = text.replace(/(\(|\))/g, '')
  }
  values.push(teamId);
  
  return call({
    text,
    values,
  })
}

router.get('/discover', authN.middleware(), async function (req, res) {
  try {
    const eventId = config.T_HACK_ID;
    const teamsAndUsers = await getTeams(eventId);
    res.send(teamsAndUsers);
  } catch (e) {
    errorHandler(e, res);
  }
});

router.post('/createTeam', authN.middleware(), async function (req, res) {
  try {
    const hackerId = req._acl.id;
    const participation = await participationDB.getById(hackerId, config.T_HACK_ID);
    if (!participation || participation.hackerstatus === 'open') {
      return res.send({error: 'Для начала нужно пройти регистрацию.'});
    }
    if (participation.team_id) {
      return res.send({error: 'У вас уже есть команда.'})
    }
    
    let createdTeamId;
    await tx(async client => {
      const {error, ...result} = await createTeam({
        ...req.body,
        leadId: hackerId,
      }, client);
      if (error) {
        return res.send({error});
      }
      createdTeamId = result.rows[0].id;
      await participationDB.setTeamId(config.T_HACK_ID, createdTeamId, hackerId, client);
    });
    if (createdTeamId)
      res.send({tean: await findTeamById(createdTeamId)});
  } catch (e) {
    errorHandler(e, res);
  }
});

router.post('/leftTeam', authN.middleware(), async (req,res) => {
  try {
    const hackerId = req._acl.id;
    const eventId = config.T_HACK_ID;
    const participation = await participationDB.getById(hackerId, config.T_HACK_ID);
    const teamId = participation.team_id;
    if (!teamId) {
      return res.send({error: 'Вы не состоите в команде.'})
    }
    
    const {teams: [team]} = await getTeams(eventId, teamId);
    if (!team) {
      return res.send({result: false});
    }
    const {teammates} = team;

    const newLead = teammates.find(({id}) => id !== hackerId);
    const newLeadId = newLead ? newLead.id : null;
    await tx(async client => {
      await updateTeamLeadId(teamId, newLeadId, client);
      await participationDB.setTeamId(config.T_HACK_ID, null, hackerId, client);
    });
    res.send({result: !!await findTeamById(teamId)});
  } catch (e) {
    errorHandler(e, res);
  }
});

router.post('/joinTeam', authN.middleware(), async (req, res) => {
  try {
    const hackerId = req._acl.id;
    const eventId = config.T_HACK_ID;

    const participation = await participationDB.getById(hackerId, eventId);
    if (!participation || participation.hackerstatus === 'open') {
      return res.send({error: 'Для начала нужно пройти регистрацию.'});
    }
    if (participation.team_id) {
      return res.send({error: 'Для начала нужно выйти из текущей команды.'})
    }
    const teamName = req.body.teamName;
    if (!teamName || typeof teamName !== 'string' || !teamName.trim()) {
      return res.send({error: 'Название команды обязательно.'})
    }
    const team = await findTeamByName(teamName.trim());
    if (!team) {
      return res.send({error: 'Команда не найдена.'})
    }
    if (!team.leadId) {
      return res.send({error: 'Команда не активна.'})
    }

    const {teammates} = (await getTeams(eventId, team.id)).teams[0];
    if (teammates.length >= team.limit) {
      return res.send({error: 'В команде нет свободных мест. Уточните информацию у капитана команды.'})
    }

    await tx(client => participationDB.setTeamId(config.T_HACK_ID, team.id, hackerId, client));
    res.send(team);
  } catch (err) {
    errorHandler(err, res);
  }
});

router.get('/myTeamProfile', authN.middleware(), async (req, res) => {
  try {
    const hackerId = req._acl.id;
    const participation = await participationDB.getById(hackerId, config.T_HACK_ID);
    if (!participation || participation.hackerstatus === 'open') {
      return res.send({error: 'Для начала нужно пройти регистрацию.'});
    }
    if (!participation.team_id) {
      return res.send({status: 'Нет команды.'});
    }
    const {teammates} = (await getTeams(config.T_HACK_ID, participation.team_id)).teams[0];
    const check_points = getCheckpoints();
    const teamInfo = await findTeamById(participation.team_id);
    res.send({
      ...teamInfo,
      teammates,
      check_points,
    });
  } catch (e) {
    errorHandler(e, res);
  }
});

router.put('/updateTeam', authN.middleware(), async (req, res) => {
  try  {
    const hackerId = req._acl.id;
    const eventId = config.T_HACK_ID;

    const participation = await participationDB.getById(hackerId, eventId);
    if (!participation || participation.hackerstatus === 'open') {
      return res.send({error: 'Для начала нужно пройти регистрацию.'});
    }

    if (!participation.team_id) {
      return res.send({error: 'Нет команды для редактирования.'});
    }
    
    const {error} = await updateTeam(participation.team_id, req.body);
    if (error) {
      return res.send({error});
    }
    
    const teamInfo = await findTeamById(participation.team_id);
    res.send(teamInfo);
  } catch (e) {
    errorHandler(e, res);
  }
});

module.exports = router;


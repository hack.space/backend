-- DROP TABLE IF EXISTS OrgActors, HackerSkillss, Participations, Events, Organizations, Skills, Hackers CASCADE;

CREATE TABLE IF NOT EXISTS Hackers (
	id serial PRIMARY KEY,
	/*contains first name and last name or pseudoname
	example: Marry Ann*/
	username text,
	/*where participant wish to get list of hackathons from in the first 
	order 
	example: Moscow*/
	city text,
	/*link to telegram profile (if public)
	example: t.me/username*/
	tgProfileLink text,
	/*email. Is used for identifing user during login
	example: marry.ann@gmail.com*/
	email text UNIQUE,
	/*true if email is verified
	example: t*/
	email_is_verified boolean,
	/*user's contact phone
	example: +7 914 230 23 23*/
	contactPhone text,
	/*true if phone is verified (by code in sms)
	example: f*/
	contact_phone_is_verified boolean,
	/*contains short description of user's activity.
	we use it as #whois tag.*/
	bio text,
	/*link to profile's picture, which is stored in 
	documents service*/
	pic text,
	/*password hash
	example: */
	pwd text,
	/*user's salt of password hash*/
	salt text,
	/*user's telegram ID
	example: 12345678*/
	tgId text,
	/*address of blockchain wallet*/
	addr text 
);

CREATE TABLE IF NOT EXISTS Organizations (
	id serial PRIMARY KEY,
	/*organization name
	example: Bright Minds Inc.*/
	title text,
	/*link to web-site
	example: www.bright-minds.com*/
	webSite text,
	/*url to companies' logo
	example: www.awesome-logo.com/some.jpg*/
	logo text
);

CREATE TABLE IF NOT EXISTS OrgActors (
	id serial PRIMARY KEY,
	/*first name and last name of an employee of an organization
	example: Anton Mishin*/
	username text,
	/*email is uniq key for login. Null is possible, it means email is
	not confirmed by a user, he/she can't login using it.
	example: example@mail.com*/
	email text UNIQUE,
	/*one of admin or basic:
	- admin can CRUD employees, and manage events
	- basic can only manage events (without any limitations yet)
	example: admin*/
	accessLevel text,
	/*password's hash*/
	pwd text,
	/*salt of the password*/
	salt text,
	/*id of organization*/
	organizationId int,
	/*incoming email being here while not confirmed by actor
	example: example@gmail.com*/
	new_email text UNIQUE not null,
	FOREIGN KEY (organizationId) REFERENCES Organizations (id)
);

CREATE TABLE IF NOT EXISTS Events (
	id serial PRIMARY KEY,
	/*event's title
	example: WhateverHack*/
	title text,
	/*date of starting of the event
	example: 2017-06-18T21:00:00Z*/
	dateStart timestamp,
	/*date of end of the event
	example: 2017-06-20T16:00:00Z*/
	dateEnd timestamp,
	/*name of city of region where hackathon will going
	example: Moscow*/
	city text,
	/*link to web-site (landing) of the event
	example: www.bright-minds.com/whateverhack*/
	link text,
	/*link to picture with defined size
	example: our-storage.org/preview/123/big*/
	preview text,
	/*id of organization*/
	organizationId int,
	/*now it is always hackathon but meetups are possible in
	the future
	example: hackathon*/
	eventType text,
	/*exact location of location in specific format
	example: lat:-20.37512536,lng:25.65785214*/
	location text,
	/*secret word for check-in
	example: secret-word-123*/
	enteringWord text, 
	/*true if event is a draft and not avaliable in calendar*/
	isDraft boolean DEFAULT true,
	/*true if admin shouldn't confirm participation, a participant
	get status confirmed directly without apllied*/
	autoconfirmation boolean DEFAULT true,
	/*true if judging is started*/
	judging_enabled boolean DEFAULT false,
	/*true if p2p is enabled*/
	p2p_enabled boolean DEFAULT false,
	p2p_coef float DEFAULT 0,
	p2p_max_count numeric default 0,
	vote_hash text,
	colors json,
	judge_logo text,
	hello_message text, /*templated with #name #title*/
	judge_star_icon_type text,
	judge_comments_enabled boolean DEFAULT false,
	data json default '{}',
	submissionDue timestamp,
	FOREIGN KEY (organizationId) REFERENCES Organizations (id)
);

CREATE TABLE IF NOT EXISTS Participations (
	hackerId int,
	eventId int,
	/*status of participation:
	- applied: 			user applied for the hackathon
	- confirmed:		participation is confirmed by organizator
	(autoconfirmation is possible in the event's section)
	- declined: if a user's application was rejected by an organizer
	- activated: 		user checked-in the hack
	- participated: user left hack after activation
	- won: 					set by organizator
	example: f*/
	hackerStatus text,
	/*amount of xp for the hack*/
	xp int,
	/*amout of coins for the hack*/
	coins float,
	/*is the user searchable by skill
	example: t*/
	isSearchable boolean DEFAULT false, 
	/*verification status:
	1_m: verified before 1 month
	1_w: verified before 1 week
	1_d: verified before 1 day
	not_sure: user is not sure about her participation
	unverified: user didn't reply on verification request
	canceled: user said that she will not take the participation
	*/
	verification_status text DEFAULT 'unverified',
	/*if user canceled from 'confirmed' hackerStatus
	the reason of should be put here
	example: "don't have time"*/
	leave_reason text,
	PRIMARY KEY (hackerId, eventId),
	FOREIGN KEY (hackerId) REFERENCES Hackers (id),
	FOREIGN KEY (eventId) REFERENCES Events (id)
);

/*consists general skiils such as "frontend" or "blockchain"*/
CREATE TABLE IF NOT EXISTS Skills (
	id serial PRIMARY KEY,
	/*title of a skill
	example: blockchain*/
	tag text
);

CREATE TABLE IF NOT EXISTS HackerSkills (
	/*reference to a hacker*/
	hackerId int,
	/*reference to general skill*/
	skillId int,
	/*legacy thing. Tbr*/
	strength int,
	/*legacy thing. Tbr*/
	countverified int,
	PRIMARY KEY (hackerId, skillId),
	FOREIGN KEY (hackerId) REFERENCES Hackers (id),
	FOREIGN KEY (skillId) REFERENCES Skills (id)
);

/*confirmation of skills by other users*/
CREATE TABLE IF NOT EXISTS SkillVerifications (
	/*reference to a hacker who put a like*/
	hacker_who int,
	/*reference to a hacker who get a like*/
	hacker_whom int,
	/*reference to general skill*/
	skill_id int,
	PRIMARY KEY (hacker_who, hacker_whom, skill_id),
	FOREIGN KEY (hacker_who) REFERENCES Hackers (id),
	FOREIGN KEY (hacker_whom) REFERENCES Hackers (id),
	FOREIGN KEY (skill_id) REFERENCES Skills (id)
);

/*messages which will be send to all participants of 
specific event*/
CREATE TABLE IF NOT EXISTS Messages (
	id serial PRIMARY KEY,
	/*reference to an event*/
  eventId int,
	/*when the message was posted
	example: 2017-06-20T12:23:21Z*/
	postTimestamp text,
	/*content of the message
	example: Lunch is ready*/
	content text,
	FOREIGN KEY (eventId) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Teams (
  id serial PRIMARY KEY,
  name text,
  event_id int,
  speech_order int NOT NULL DEFAULT 0,
  hide_list json DEFAULT '[]',
  FOREIGN KEY (event_id) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Judges (
  id serial PRIMARY KEY,
  name text,
  email text,
  token text,
	status text NOT NULL DEFAULT 'created',
  event_id int,
  /* 0 - judge, 1 - participant (p2p) */
  type int default 0,
  profile_id int,
	FOREIGN KEY (event_id) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Criterias (
  id serial PRIMARY KEY,
  name text,
  description text,
  scale_from int,
  scale_to int,
  event_id int,  
  poly_title boolean DEFAULT false,
  weight numeric(5,2) DEFAULT 1,
	FOREIGN KEY (event_id) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Verdicts (
  team_id int,
  judge_id int,
  criteria_id int,
  score int,
	PRIMARY KEY (team_id, judge_id, criteria_id),
	FOREIGN KEY (team_id) REFERENCES Teams (id),
	FOREIGN KEY (judge_id) REFERENCES Judges (id),
	FOREIGN KEY (criteria_id) REFERENCES Criterias (id)
);

CREATE TABLE IF NOT EXISTS JudgeComments (
  judge_id int,
  team_id int, 
  comment text,
	PRIMARY KEY (team_id, judge_id),
  FOREIGN KEY (team_id) REFERENCES Teams (id),
	FOREIGN KEY (judge_id) REFERENCES Judges (id)
)

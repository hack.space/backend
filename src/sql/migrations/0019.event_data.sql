alter table events add column data json default '{}';
alter table events add column submissionDue timestamp;

alter table events drop column description;
alter table events drop column schedule;
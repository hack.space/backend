CREATE TABLE IF NOT EXISTS Teams (
  id serial PRIMARY KEY,
  name text,
  event_id int,
	FOREIGN KEY (event_id) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Judges (
  id serial PRIMARY KEY,
  name text,
  email text,
  judging_link text,
  event_id int,
	FOREIGN KEY (event_id) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Criterias (
  id serial PRIMARY KEY,
  name text,
  description text,
  scale_from int,
  scale_to int,
  event_id int,  
	FOREIGN KEY (event_id) REFERENCES Events (id)
);

CREATE TABLE IF NOT EXISTS Verdicts (
  team_id int,
  judge_id int,
  criteria_id int,
  score int,
	PRIMARY KEY (team_id, judge_id, criteria_id),
	FOREIGN KEY (team_id) REFERENCES Teams (id),
	FOREIGN KEY (judge_id) REFERENCES Judges (id),
	FOREIGN KEY (criteria_id) REFERENCES Criterias (id)
);


alter table participations add column verification_status text DEFAULT 'unverified';
alter table participations add column leave_reason text;
alter table participations add column status_on_hold text;
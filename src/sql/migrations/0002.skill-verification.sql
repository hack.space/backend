/*confirmation of skills by other users*/
CREATE TABLE IF NOT EXISTS SkillVerifications (
	/*reference to a hacker who put a like*/
	hacker_who int,
	/*reference to a hacker who get a like*/
	hacker_whom int,
	/*reference to general skill*/
	skill_id int,
	PRIMARY KEY (hacker_who, hacker_whom, skill_id),
	FOREIGN KEY (hacker_who) REFERENCES Hackers (id),
	FOREIGN KEY (hacker_whom) REFERENCES Hackers (id),
	FOREIGN KEY (skill_id) REFERENCES Skills (id)
);

alter table hackerskills drop strength;
alter table hackerskills drop countverified;
alter table events add autoconfirmation boolean;
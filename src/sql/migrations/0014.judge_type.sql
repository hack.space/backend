alter table judges
add column type int default 0;

alter table events
add column p2p_enabled boolean default false;

alter table events
add column p2p_coef float default 0;

alter table judges
add column profile_id int;
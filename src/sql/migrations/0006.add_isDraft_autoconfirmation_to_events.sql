alter table events
add isDraft boolean DEFAULT true,
add autoconfirmation boolean DEFAULT true;
-- DROP TABLE IF EXISTS OrgActors, HackerSkillss, Participations, Events, Organizations, Skills, Hackers CASCADE;

CREATE TABLE IF NOT EXISTS Hackers (
	id serial PRIMARY KEY,
	/*contains first name and last name or pseudoname
	example: Marry Ann*/
	username text,
	/*where participant wish to get list of hackathons from in the first 
	order 
	example: Moscow*/
	city text,
	/*link to telegram profile (if public)
	example: t.me/username*/
	tgProfileLink text,
	/*email. Is used for identifing user during login
	example: marry.ann@gmail.com*/
	email text UNIQUE,
	/*true if email is verified
	example: t*/
	email_is_verified boolean,
	/*user's contact phone
	example: +7 914 230 23 23*/
	contactPhone text,
	/*true if phone is verified (by code in sms)
	example: f*/
	contact_phone_is_verified boolean,
	/*contains short description of user's activity.
	we use it as #whois tag.*/
	bio text,
	/*link to profile's picture, which is stored in 
	documents service*/
	pic text,
	/*password hash
	example: */
	pwd text,
	/*user's salt of password hash*/
	salt text,
	/*user's telegram ID
	example: 12345678*/
	tgId text,
	/*address of blockchain wallet*/
	addr text 
);

CREATE TABLE IF NOT EXISTS Organizations (
	id serial PRIMARY KEY,
	/*organization name
	example: Bright Minds Inc.*/
	title text,
	/*link to web-site
	example: www.bright-minds.com*/
	webSite text
);

CREATE TABLE IF NOT EXISTS OrgActors (
	id serial PRIMARY KEY,
	/*first name and last name of an employee of an organization
	example: Anton Mishin*/
	username text,
	/*one of admin or basic:
	- admin can CRUD employees, and manage events
	- basic can only manage events (without any limitations yet)
	example: admin*/
	accessLevel text,
	/*password's hash*/
	pwd text,
	/*salt of the password*/
	salt text,
	/*id of organization*/
	organizationId int,
	FOREIGN KEY (organizationId) REFERENCES Organizations (id)
);

CREATE TABLE IF NOT EXISTS Events (
	id serial PRIMARY KEY,
	/*event's title
	example: WhateverHack*/
	title text,
	/*date of starting of the event
	example: 2017-06-18T21:00:00Z*/
	dateStart timestamp,
	/*date of end of the event
	example: 2017-06-20T16:00:00Z*/
	dateEnd timestamp,
	/*name of city of region where hackathon will going
	example: Moscow*/
	city text,
	/*general description
	example: Hack whaterver you want and get $1M*/
	description text,
	/*link to web-site (landing) of the event
	example: www.bright-minds.com/whateverhack*/
	link text,
	/*link to picture with defined size
	example: our-storage.org/preview/123/big*/
	preview text,
	/*id of organization*/
	organizationId int,
	/*now it is always hackathon but meetups are possible in
	the future
	example: hackathon*/
	eventType text,
	/*exact location of location in specific format
	example: lat:-20.37512536,lng:25.65785214*/
	location text,
	/*secret word for check-in
	example: secret-word-123*/
	enteringWord text, 
	/*special section for schedule*/
	schedule text, 
	/*set true if no validation by organizators*/
	autoconfirmation boolean,
	FOREIGN KEY (organizationId) REFERENCES Organizations (id)
);

CREATE TABLE IF NOT EXISTS Participations (
	hackerId int,
	eventId int,
	/*status of participation:
	- applied: 			user applied for the hackathon
	- confirmed:		participation is confirmed by organizator
	(autoconfirmation is possible in the event's section)
	- verified: 		user verified his/her participation in the hack
	- activated: 		user checked-in the hack
	- participated: user left hack after activation
	- won: 					set by organizator
	example: f*/
	hackerStatus text,
	/*amount of xp for the hack*/
	xp int,
	/*amout of coins for the hack*/
	coins float,
	/*is the user searchable by skill
	example: t*/
	isSearchable boolean DEFAULT false, /*new*/
	PRIMARY KEY (hackerId, eventId),
	FOREIGN KEY (hackerId) REFERENCES Hackers (id),
	FOREIGN KEY (eventId) REFERENCES Events (id)
);

/*consists general skiils such as "frontend" or "blockchain"*/
CREATE TABLE IF NOT EXISTS Skills (
	id serial PRIMARY KEY,
	/*title of a skill
	example: blockchain*/
	tag text
);

CREATE TABLE IF NOT EXISTS HackerSkills (
	/*reference to a hacker*/
	hackerId int,
	/*reference to general skill*/
	skillId int,
  strength int,
  countverified int,
	PRIMARY KEY (hackerId, skillId),
	FOREIGN KEY (hackerId) REFERENCES Hackers (id),
	FOREIGN KEY (skillId) REFERENCES Skills (id)
);

/*messages which will be send to all participants of 
specific event*/
CREATE TABLE IF NOT EXISTS Messages (
	id serial PRIMARY KEY,
	/*reference to an event*/
  eventId int,
	/*when the message was posted
	example: 2017-06-20T12:23:21Z*/
	postTimestamp text,
	/*content of the message
	example: Lunch is ready*/
	content text,
	FOREIGN KEY (eventId) REFERENCES Events (id)
);
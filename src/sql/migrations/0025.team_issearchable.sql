alter table teams add column is_searchable bool default true;
alter table teams add column idea text;
alter table teams add column repo text;
alter table teams add column slides text;

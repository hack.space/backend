alter table orgactors add new_email text UNIQUE;
update orgactors set new_email = email;
alter table orgactors alter new_email set not null;
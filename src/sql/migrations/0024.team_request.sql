alter table teams add column twit text;
alter table teams add column max_members int default 5;
alter table teams add column teamlead_id int;

alter table participations add column team_id int;
alter table participations add column team_approval bool;
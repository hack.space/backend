alter table events add column judge_comments_enabled text DEFAULT false;

CREATE TABLE IF NOT EXISTS JudgeComments (
  judge_id int,
  team_id int, 
  comment text,
	PRIMARY KEY (team_id, judge_id),
  FOREIGN KEY (team_id) REFERENCES Teams (id),
	FOREIGN KEY (judge_id) REFERENCES Judges (id)
);
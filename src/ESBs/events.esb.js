const eventRepo = require('../controllers/event.controller');
const judgeRepo = require('../controllers/judge.controller');
const participationRepo = require('../controllers/participation.controller');

const hackerEsb = require('../ESBs/hackers');

const activateSchema = require('../../apidoc/schemas/participation/activate.schema.json');
const applySchema = require('../../apidoc/schemas/events/apply.schema.json');

const { validate } = require('../lib/validator');
const { ADMIN_SECRET, CHECKIN_DIST, SECRET } = require('../config');
const { makeToken } = require('../plugins/authN');
const { makeVotingLink, makeRestorePwdLink } = require('../lib/utils/links');
const calcDistance = require('../lib/utils/calc-distance');
const { NotFoundError, BadRequestError } = require('../plugins/errorHandler');
const { decore } = require('../lib/core-decorators');
const { VERIFICATION_STATUS } = participationRepo;
const randPwd = require('../lib/utils/rand-pwd');
const confirmer = require('../lib/workers/confirmer');
const { sendRowEmail } = require('../lib/mailer');
const { createHash } = require('../lib/utils/hash'); 
const jwt = require('jsonwebtoken');

const unit = {};

const mapEvent = e => ({
  ...e.data,
  id: e.id,
  title: e.title,
  city: e.city,
  dateStart: e.datestart.toISOString(),
  dateEnd: e.dateend.toISOString(),
  submissionDue: e.submissiondue && e.submissiondue.toISOString() || e.datestart.toISOString(),
  link: e.link,
  preview: e.preview,
  eventType: e.eventtype,
  status: e.hackerstatus,
  isSearchable: e.issearchable,
  judgingEnabled: e.judging_enabled,
  p2pEnabled: e.p2p_enabled,
  organization: {
    id: e.org_id,
    title: e.org_title,
    website: e.org_website
  }
});

unit.getList = async (hackerId, params) => {
  const limit = params.count || 10;
  const offset = params.page ? limit * params.page : 0;
  const status = params.status || "open";

  const events = await eventRepo.selectMany(hackerId, status, limit, offset);

  return events.map(mapEvent);
}

/**
  * Fetch event info.
  * 
  * @param {number} eventId
  * @param {number?} hackerId
  */
unit.getById = async (eventId, hackerId) => {
  const event = await eventRepo.getEventById(eventId, hackerId);
  if (!event)
    throw new NotFoundError();
  let participation = { hackerstatus: 'open' };
  if (hackerId) {
    participation = await participationRepo.getById(hackerId, eventId);
  }
  let output = mapEvent({ ...event, ...participation });

  // TODO: generate link by request
  output.judgingLink = null;
  if (output.p2pEnabled && output.status === 'activated') {
    output.judgingLink = await generateJudgingLink(eventId, hackerId, output.organization.id);
    console.debug('outputJudgingLink:', output.judgingLink);
  }

  // await validate(output, eventSchema, 500); // TODO: Uncomment
  return output;
}

unit.getListById = (id) => {
  return eventRepo.getAllEventsOfOrganization(id);
}

/**
 * @param {number} hackerId
 * @param {eventId} eventId
 * @param {object?} applicantInfo
 */
unit.apply = async (hackerId, eventId, applicantInfo) => {
  const event = await eventRepo.getEventById(eventId, hackerId);
  if (!event)
    throw new NotFoundError();
  if (hackerId) {
    await participationRepo.create(hackerId, eventId, 'applied');
  } else {
    await validate(applicantInfo, applySchema);

    const newHacker = {
      username: applicantInfo.username,
      email: applicantInfo.email,
      pwd: randPwd(),
    };

    if (applicantInfo.phone) {
      newHacker.contactPhone = applicantInfo.phone;
    }
    
    const createdHacker = await hackerEsb.create(newHacker, { verifyEmail: false });

    // generates code of type xxxxxxxx-xxxxxxxx-xxxxxxxx
    const code = [1, 2, 3].map(() => randPwd()).join('-');
    await confirmer.set(createdHacker.email, {
      code: createHash(code),
      id: createdHacker.id,
      expiresin: event.dateEnd || (Date.now() + 2 * 24 * 60 * 1000), // till date end or 2 days
    });
  
    const codeWithEmail = jwt.sign({
      code,
      email: createdHacker.email,
    }, SECRET);
  
    const restoreUrl = makeRestorePwdLink(codeWithEmail, true);
    sendRowEmail(createdHacker.email, `${event.title} application`, `
      Hello, ${newHacker.username}!

      You have successfully applied for ${event.title} hackathon.
      For tracking your application status please use your Hackspace account:

      ${restoreUrl}

      Best regards,
      Your Hackspace Team.
    `);

    await participationRepo.create(createdHacker.id, eventId, 'applied');
  }
}

unit.toggleIsSearchable = async (hackerId, eventId) => {
  if (typeof parseInt(hackerId) !== 'number' || typeof parseInt(eventId) !== 'number')
    throw new BadRequestError();

  const updated = await participationRepo.toggleSearchble(hackerId, eventId)
  if (!updated)
    throw new NotFoundError();
  return { isSearchable: updated.issearchable };
}

unit.activate = async (hackerId, eventId, data) => {
  await validate(data, activateSchema)

  let activated = false;

  const participation = await participationRepo.getById(hackerId, eventId);
  if (!participation)
    throw new NotFoundError();
  const participantStatus = participation.hackerstatus;

  switch (participantStatus) {
    case 'activated':
      return { status: 'already' };
    case 'participated':
      await participationRepo.update(hackerId, eventId, 'activated');
      return { status: 'activated' };
    case 'open':
      throw new NotFoundError();
  }

  if (!data.location && !data.enteringWord)
    return Promise.reject({ status: "failed" });

  const { location, enteringword: key } = await eventRepo.getEventSecrets(eventId);

  if (data.location && location) {
    let rg = /lat\:([\d\-\.]+),lng\:([\d\-\.]+)/i
    let [, lat, lng] = rg.exec(location); // event's location
    let { lat: userLat, lng: userLng } = data.location; // user's location

    let dist = calcDistance(lat, lng, userLat, userLng, 'K');

    activated = dist <= CHECKIN_DIST; // in 100 metres
  } else if (data.enteringWord && key) {
    activated = data.enteringWord === key;
  }

  if (!activated)
    return Promise.resolve({ status: 'failed' });

  await participationRepo.update(hackerId, eventId, 'activated', 0, 0);
  return Promise.resolve({ status: 'activated' });
}

/**
 * @deprecated use leave instead
 */
unit.finish = async (hackerId, eventId) => {
  const participation = await participationRepo.getById(hackerId, eventId);
  if (!participation)
    throw new NotFoundError();

  const participantStatus = participation.hackerstatus;

  switch (participantStatus) {
    case 'activated':
      return { status: 'failed' };
    case 'participated':
      return { status: 'already' };
    case 'open':
      throw new NotFoundError();
  }

  await participationRepo.update(hackerId, eventId, 'participated', 0, 0);
  return { status: 'participated' };
}

unit.leave = async (hackerId, eventId, { reason }) => {
  if (typeof parseInt(hackerId) !== 'number' || typeof parseInt(eventId) !== 'number')
    throw new BadRequestError();
  const participation = await participationRepo.getById(hackerId, eventId);
  if (!participation)
    throw new NotFoundError();

  const participantStatus = participation.hackerstatus;

  switch (participantStatus) {
    case 'applied':
      await participationRepo.remove(hackerId, eventId);
      return { status: 'open' };
    case 'confirmed':
      if (!reason)
        throw new BadRequestError('Reason is required');
      await participationRepo.updateVerificationStatus(hackerId, eventId, VERIFICATION_STATUS.cancelled, reason);
      return { status: 'confirmed' };
    case 'activated':
      await participationRepo.update(hackerId, eventId, 'participated', 0, 0);
      return { status: 'participated' };
    case 'participated':
      return { status: 'participated' };
    case 'open':
      throw new NotFoundError();
    default:
      return { status: 'failed' };
  }
}

async function generateJudgingLink(eventId, hackerId, orgId) {
  console.debug('generateJudgingLink:' + `${eventId},${hackerId}`)
  const judge = await judgeRepo.get(hackerId, eventId);
  console.debug(JSON.stringify(judge));
  if (judge && judge.token)
    return makeVotingLink(judge.token);

  const hacker = await hackerEsb.getById(hackerId, hackerId);
  console.debug(JSON.stringify(hacker));

  let obj = {
    name: hacker.username,
    email: hacker.email,
    eventId,
    profileId: hackerId,
  };

  let createQueryRes = await judgeRepo.insert(obj);
  obj.id = createQueryRes.rows[0].id;

  const token = makeToken({
    id: obj.id,
    orgId,
    eventId,
    role: 'judge',
    type: 1 // participant (peer)
  }, '60d', ADMIN_SECRET);

  await judgeRepo.updateToken(obj.id, token);
  console.debug('inserted');

  return makeVotingLink(token);
}

module.exports = decore(unit);
const db = require('../controllers/skill.controller');

let unit = {};

unit.getList = () => {
  return db.getList();
}

module.exports = unit;
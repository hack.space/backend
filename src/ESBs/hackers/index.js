const hackerDb = require('../../controllers/hacker.controller');
const hackerSkillDb = require('../../controllers/hackerSkill.controller');
const { validate } = require('../../lib/validator');
const { generatePasswordHash, makeToken } = require('../../plugins/authN');
const mappers = require('./mappers');
const mailer = require('../../lib/mailer');
const { decore } = require('../../lib/core-decorators'); // adds try-catch blocks to all functions
const confirmer = require('../../lib/workers/confirmer');
const {
  EMAIL_CHECK_EXPIRATION_TIME: EXPIRATION_TIME,
  EMAIL_CHECK_RESEND_TIME_LIMIT: RESEND_TIME_LIMIT
} = require('../../config');
// json schemas
const createSchema = require('../../../apidoc/schemas/hackers/create.schema.json');
const updateSchema = require('../../../apidoc/schemas/hackers/update.schema.json');

// exported object
let unit = {};

/**
 * Fetches list of hackers sorted by wins or xp.
 * @param {string} sortBy - wins or xp (default)
 * @param {number} page - starting from 0
 * @param {number} count - how much items to deliver
 * @param {number} eventId - that's clear
 * @returns {object[]} list of hackers with general info about them
 */
unit.getList = async ({ sortBy, page, count, eventId }) => {
  sortBy = sortBy === 'wins' ? 'wins' : 'xp';
  page = isNaN(parseInt(page)) ? 0 : parseInt(page);
  count = isNaN(parseInt(count)) ? 100 : parseInt(count);

  let { rows: hackers } = await hackerDb.selectMany(sortBy, page, count, eventId);
  return mappers.outputListMapper(hackers, Boolean(eventId));
}

/**
 * Fetches hacker's object by ID.
 * @param {number} id of a hacker
 * @param {boolean} isMe set true if a user requests
 *   information about himself. Contact info will be available
 *   in that case.
 */
unit.getById = async (id, curUserId) => {
  id = typeof id === 'string' && id.toLowerCase() === 'me' ? +curUserId : +id;
  let isMe = id == curUserId;

  let getQueryRes = await hackerDb.selectOne(id);
  if (getQueryRes.rowCount === 0)
    throw { code: 404 }
  let hacker = getQueryRes.rows[0];
  let { rows: skills } = await hackerSkillDb.selectMany(hacker.id, curUserId);
  return mappers.outputSingleMapper(hacker, skills, isMe)
}

/**
 * Sign Up.
 * @param {object} hacker hacker's object
 * @param {object} options
 * @param {boolean} options.verifyEmail send email verification code - default: true
 * @returns {promise} that resolves created hacker's object
 */
unit.create = async (hacker, { verifyEmail = true }) => {
  // the validation is like firewall, it removes not expected fields as well
  await validate(hacker, createSchema)

  let { pwdHash = '', salt = '' } = generatePasswordHash(hacker.pwd);
  hacker.pwd = pwdHash
  hacker.salt = salt

  // email should be verified first, before adding into the db
  let email = hacker.email.toLowerCase();
  hacker.email_is_verified = false;
  // creates new row in the db
  let { id } = (await hackerDb.insert(hacker)).rows[0];

  if (verifyEmail) {
    sendEmailVerificationCode(email, hacker.username, id, 'UPDATE');
  }
  // populates skills
  await hackerSkillDb.create(id, hacker.skills);
  // returns the created object
  return await unit.getById(id, id);
}

/**
 * Updates a hacker with ID.
 * @param {number} id of a hacker
 * @param {object} obj - updated object (now all fields are required)
 * @returns {object} updated hacker
 */
unit.update = async (id, obj) => {
  await validate(obj, updateSchema);
  let hacker = (await hackerDb.selectOne(id)).rows[0]
  if (!hacker)
    throw { code: 404 }
  if (obj.email && obj.email !== hacker.email) {
    if (hacker.tgid) {
      obj.email = obj.email.toLowerCase();
      if (obj.email === hacker.email && !hacker.email_is_verified)
        unit.resendConfirmCode(obj.email).catch(err => console.debug(err))
      else if (obj.email !== hacker.email) {
        obj.email_is_verified = false;
      }
    } else
      throw { code: 400, message: 'Cannot overwrite identifier.' }
  }
  await hackerDb.update(id, obj);
  if (obj.skills)
    await hackerSkillDb.update(id, obj.skills);

  if (obj.email && obj.email !== hacker.email)
    sendEmailVerificationCode(obj.email, hacker.username, id, 'UPDATE')

  return unit.getById(id, id);
}

unit.verifySkill = async (curUserId, hackerId, skillId) => {
  if (+hackerId != hackerId || +skillId != skillId || curUserId == hackerId)
    throw { code: 404 };

  let verifyQueryRes = await hackerSkillDb.addVerification(curUserId, hackerId, skillId, hackerSkillDb.deleteVerification);
  return {
    hackerId: +hackerId,
    skillId: +skillId,
    actionMade: verifyQueryRes.command === 'INSERT' ? 'like' : 'dislike',
  };
}

/**
 * Verifies hacker's account through email.
 * @param {number} id of an uncofirmed hacker's account
 * @param {string} email which need to be confirmed
 * @returns {promise} that will resolve hacker's token
 */
unit.confirmEmail = async (email, code) => {
  let row = await confirmer.get(email)
  if (!row)
    throw { code: 404 }
  if (row.checked >= 5) {
    await confirmer.del(email);
    afterDelete(row)
    throw { code: 404 }
  }
  if (row.code != code)
    throw { code: 401 }
  await confirmer.del(email);
  await hackerDb.update(row.id, { email_is_verified: true })
  return {
    token: makeToken({ id: row.id, role: 'hacker' }),
    hacker: await unit.getById(row.id, row.id)
  }
}

unit.resendConfirmCode = async (email) => {
  let row = await confirmer.get(email, false)
  if (!row)
    throw { code: 404 }
  let pastTime = Date.now() - (row.expiresin - EXPIRATION_TIME)
  if (pastTime < RESEND_TIME_LIMIT)
    throw { code: 429, retryAfter: RESEND_TIME_LIMIT - pastTime }
  sendEmailVerificationCode(email, row.username, row.id, row.onDel)
  return Promise.resolve()
}

/**
 * generates and sends a confirmation code
 * @param {string} email key field
 * @param {string} username in greeting of letter
 * @param {string} id hacker id
 * @param {string?} onDel DELETE or UPDATE a row with email
 */
function sendEmailVerificationCode(email, username, id, onDel = 'DELETE') {
  const code = (Math.random() * 1e5).toFixed(0).padStart(5, '0')
  mailer.sendMail(email, { name: username, code })
    .then(() => confirmer.set(email, { code, id, onDel }))
}

// TDR
function afterDelete(data) {
  // TODO: bad logical injection (!DRY)
  if (data.onDel === 'UPDATE')
    return hackerDb.update(data.id, { email: null })
  else if (data.onDel === 'DELETE')
    return hackerDb.remove(data.id)
}

module.exports = decore(unit);

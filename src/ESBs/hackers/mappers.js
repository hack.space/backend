let unit = {};

/**
 * Maps flat object from DB to structured object for API.
 * @param {object} hacker hacker object from db
 * @param {object[]} skills list of row skills
 * @param {boolean} isMe set true if a user requests
 *   information about himself. Contact info will be available
 *   in that case.
 * @returns {object} mapped json object
 */
unit.outputSingleMapper = (hacker, skills, isMe) => {
  let output = {
    id: hacker.id,
    username: hacker.username,
    pic: hacker.pic,
    bio: hacker.bio,
    city: hacker.city,
    stat: {
      hackTotal: +hacker.hackTotal,
      hackWin: +hacker.hackWin,
      xp: +hacker.xp,
      coins: +hacker.coins
    },
    skills: [],
    slackDeepLink: hacker.slackDeepLink || hacker.slackdeeplink,
  }

  // show contacts if it is owner of data.
  if (isMe) {
    output.tgProfileLink = hacker.tgprofilelink,
    output.email = hacker.email;
    output.emailIsVerified = hacker.email_is_verified;
    output.contactPhone = hacker.contactphone;
    output.contactPhoneIsVerified = hacker.contact_phone_is_verified;
    output.slackId = hacker.slackId || hacker.slackid;
  }

  for (const skill of skills) {
    let mappedSkill = {
      id: skill.id,
      tag: skill.tag,
      verified: +skill.verified
    }

    if (!isMe)
      mappedSkill.liked = skill.liked;

    output.skills.push(mappedSkill);
  }

  return output;
}

/**
 * Maps db object to leaderboard app object.
 * @param {object[]} inputs database objects
 * @param {boolean} showTgProfileLink
 * @returns {object[]} mapped objects
 */
unit.outputListMapper = (inputs, showTgProfileLink) => {
  // TODO: limit access to tgProfileLink field (open for bot only)
  let output = [];
  for (let input of inputs)
    output.push({
      id: input.id,
      pic: input.pic,
      username: input.username,
      xp: +input.xp,
      wins: +input.wins,
      tgProfileLink: showTgProfileLink ? input.tgprofilelink : undefined,
      position: +input.position,
      status: input.hackerstatus,
      isSearchable: input.issearchable,
      skills: input.skills,
      slackId: input.slackid,
      slackDeepLink: input.slackdeeplink,
    })
  return output;
}

module.exports = unit;

const hackerDb = require('../controllers/hacker.controller');
const { createHmac } = require('crypto');
const hackerESB = require('../ESBs/hackers');
const { validate } = require('jsonschema');
const loginBasicSchema = require('../../apidoc/schemas/login/basic.schema.json');
const { generatePasswordHash, makeToken } = require('../plugins/authN');
const { NotFoundError, NotAuthorizedError, BadRequestError } = require('../plugins/errorHandler');
const confirmer = require('../lib//workers/confirmer');
const rand8 = require('../lib/utils/rand-pwd');
const { createHash, compareHash } = require('../lib/utils/hash');
const { makeRestorePwdLink } = require('../lib/utils/links');
const { SECRET } = require('../config');
const { sendRowEmail } = require('../lib/mailer')
const jwt = require('jsonwebtoken');

const BOT_TOKEN = process.env.TG_BOT_TOKEN;

if (!BOT_TOKEN) {
  console.error('Environment variable TG_BOT_TOKEN is required.');
  process.exit(-1);
}

let unit = {}; // this module for export;

const secretKey = createHash(BOT_TOKEN);

/**
 * Checks that message is from Telegram.
 * @param {object} param payload from telegram
 * @returns {boolean} true if signature is valid 
 */
function checkSignature({ hash, ...data }) {
  console.debug(hash);
  const checkString = Object.keys(data)
    .sort()
    .map(k => `${k}=${data[k]}`)
    .join('\n');
  console.debug(checkString);
  const hmac = createHmac('sha256', secretKey)
    .update(checkString)
    .digest('hex');
  console.debug(hmac);
  return hmac === hash;
}

/**
 * Login using email and password.
 * @param {object} body hacker's object
 * @param {object} params query parameters
 * @returns {promise} will reject with 401 code if email or password is incorrect
 *  and will resolve with jwt in token field.
 */
unit.basicLogin = async (body, params = {}) => {
  console.log(`New basic loging attempt`);
  const fields = params.fields && /^(\w+,?)+$/.exec(params.fields) ? params.fields.split(',') : [];
  const includeHackerObj = fields.indexOf('hacker') !== -1;

  const validationResult = validate(body, loginBasicSchema);
  if (!validationResult.valid) // validate against json schema
    throw new NotAuthorizedError();

  const { email, pwd } = body;
  const { rows } = await hackerDb.selectOneWhereEmail(email);
  if (rows.length === 0)
    throw new NotAuthorizedError();

  const hacker = rows[0];
  const { pwdHash } = generatePasswordHash(pwd, hacker.salt);
  if (pwdHash !== hacker.pwd)
    throw new NotAuthorizedError();

  const result = { token: makeToken({ id: hacker.id, role: 'hacker' }) };
  if (includeHackerObj)
    result.hacker = await hackerESB.getById(hacker.id);

  return result;
}

/**
 * Creates and fetches new user based on data from Telegram. 
 * @param {object} body hacker's object
 * @param {object} params query parameters
 * @returns {promise} contains an object with jwt in the "token" field.
 */
unit.telegramLogin = async (body, params) => {
  let fields = params.fields && /^(\w+,?)+$/.exec(params.fields) ? params.fields.split(',') : [];
  let includeHackerObj = fields.indexOf('hacker') !== -1;

  if (!checkSignature(body))
    throw new NotAuthorizedError('Not signed data from Telegram.');

  let { rows } = await hackerDb.selectOneWhereTgId(body.id);

  if (rows.length === 0) { // if no user -> create one
    var id = (await hackerDb.insert({
      pwd: '',
      skills: [],
      tgId: body.id,
      username: ((body.first_name || '') + ' ' + (body.last_name || '')).trim(),
      pic: body.photo_url,
      tgProfileLink: body.username ? `https://t.me/${body.username}` : ''
    })).rows[0].id;
  } else {
    var id = rows[0].id;
  }

  let result = { token: makeToken({ id, role: 'hacker' }) };
  if (includeHackerObj)
    result.hacker = await hackerESB.getById(id);

  return result;
}

/**
 * @param email
 * @param isNew indicates is user a new user, or loyal
 */
unit.requestPasswordRestore = async (email) => {
  if (!email) {
    throw new BadRequestError('Email is required');
  }

  const { rows } = await hackerDb.selectOneWhereEmail(email, true);
  if (rows.length === 0)
    throw new NotFoundError("User with that e-mail was not found");

  const hacker = rows[0];

  // generates code of type xxxxxxxx-xxxxxxxx-xxxxxxxx
  const code = [1, 2, 3].map(() => rand8()).join('-');
  await confirmer.set(email, {
    code: createHash(code),
    id: hacker.id,
  });

  const codeWithEmail = jwt.sign({
    code,
    email: hacker.email,
  }, SECRET);

  const restoreUrl = makeRestorePwdLink(codeWithEmail);
  sendRowEmail(hacker.email, `Password reset`, `
    Hello, ${hacker.username}!

    Your restore password link:
    
    ${restoreUrl}

    It will be active during 15 minutes.

    Best regards,
    Your Hackspace Team.
  `);

  return { result: true }
}

unit.setNewPassword = async (code, password) => {
  console.debug('setNewPassword: code is ', code);

  const parsed = jwt.verify(code, SECRET);
  const confirmObj = await confirmer.get(parsed.email, true);

  if (!confirmObj)
    throw new BadRequestError();

  if (confirmObj.checked > 10) {
    await confirmer.del(parsed.email);
    throw new BadRequestError();
  }

  if (compareHash(parsed.code, confirmObj.code))
    throw new BadRequestError();

  confirmer.del(parsed.email);

  const { rows } = await hackerDb.selectOneWhereEmail(parsed.email, true);
  if (rows.length === 0)
    throw new BadRequestError();

  const hacker = rows[0];

  const { pwdHash } = generatePasswordHash(password, hacker.salt);
  await hackerDb.update(hacker.id, { 
    pwd: pwdHash,
    email_is_verified: true,
  });

  const { token } = await unit.basicLogin({
    email: hacker.email,
    pwd: password,
  })

  return { 
    result: true, 
    token,
  };
}

module.exports = unit;
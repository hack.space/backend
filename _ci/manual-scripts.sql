-- insert default skills

INSERT INTO Skills (tag)
VALUES 
	('Frontend'),
	('Backend'),
	('Designer'),
	('Blockchain');

-- create default organization

INSERT INTO Organizations (title)
VALUES
  ('SYSTEM');

-- create TelegramLogin event

INSERT INTO Events (title, eventType, orgId)
VALUES
  ('TelegramLogin', 'insystem-activity', 1);


insert into organizations (title)
values ('actum'),('ai-community'), ('russian hackers');

insert into events (title, datestart, dateend, eventtype, location, enteringword, city, organizationid) values
('FutureHack', '2018-06-18T18:00:00+03:00', '2018-06-20T21:00:00+03:00', 'hackathon', 'lat:59.96904,lng:30.31565', 'startups', 'Saint-Petersburg', 4),
('Apla Blockchain Hack', '2018-07-13T18:00:00+03:00', '2018-07-15T21:00:00+03:00', 'hackathon', 'lat:59.96904,lng:30.31565', 'startups', 'Moscow', 4),
('Hack.Moscow', '2019-04-20T18:00:00+03:00', '2019-04-22T18:00:00+03:00', 'hackathon', 'lat:59.96904,lng:30.31565', 'startups', 'Moscow', 5),
('Sibur Hack', '2018-05-19T10:00:00+03:00', '2018-05-20T21:00:00+03:00', 'hackathon', 'lat:59.96904,lng:30.31565', 'startups', 'Moscow', 6);

insert into events (title, datestart, dateend, eventtype, location, enteringword, city, organizationid) values
('FutureHack', '2018-06-18T18:00:00+03:00', '2018-06-20T21:00:00+03:00', 'hackathon', 'lat:59.96904,lng:30.31565', 'startups', 'Saint-Petersburg', 1),

ALTER TABLE Participations 
ADD isSearchable boolean DEFAULT false;

ALTER TABLE Events
ADD location text,
ADD enteringWord text,
ADD schedule text;

-- up to 0.2.0

ALTER TABLE Hackers
ADD city text;

ALTER TABLE Events
ADD city text;
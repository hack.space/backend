let dbQueries = require('../src/db-queries');

let hackers = [
    {
        "username": "Jose Gomez",
        "email": "per.amor.maltsev@gmail.com",
        "bio": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
        "skill": "Motion Designer",
        "pic": "https://api.adorable.io/avatars/159/abott@adorable.png",
        "pwd": require('md5')('facepalm')
    },
    {
        "username": "Robert Martin",
        "email": "aka@gmail.com",
        "bio": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
        "skill": "Frontend Developer",
        "pic": "https://api.adorable.io/avatars/159/abott@adorable.png",
        "pwd": require('md5')('facepalm')
    },
    {
        "username": "Alex Smith",
        "email": "durov@gmail.com",
        "bio": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
        "skill": "Backend Developer",
        "pic": "https://api.adorable.io/avatars/159/abott@adorable.png",
        "pwd": require('md5')('facepalm')
    }
]

for (let hacker of hackers) {
    dbQueries.addHacker(hacker)
        .then(console.log(hacker.username + ' added'))
}

let organization = {
    "title": "Russian Hackers",
    "webSite": "rusianhacker.org",
    "pwd": require('md5')('facepalm')
}

dbQueries.addOrganization(organization)
    .then(res => console.log('org added'));

let events = [
    {
        title: "Hack.Moscow",
        dateStart: "2018-04-20",
        dateEnd: "2018-04-22",
        link: "http://hack.moscow",
        preview: "",
        organizationId: 1
    },
    {
        title: "Other greate hack",
        dateStart: "2018-05-20",
        dateEnd: "2018-05-22",
        link: "http://hack.moscow",
        preview: "",
        organizationId: 1
    }
]

for (let event of events) {
    dbQueries.addEvent(event)
        .then(res => console.log(event.title + ' added'));
}

let participations = [
    {
        hackerId: 1,
        eventId: 1,
        hackerStatus: 'wins',
        xp: 200,
        coins: 20
    }, {
        hackerId: 1,
        eventId: 2,
        hackerStatus: 'accepted',
        xp: 0,
        coins: 0
    }, {
        hackerId: 2,
        eventId: 1,
        hackerStatus: 'participated',
        xp: 15,
        coins: 0
    }, {
        hackerId: 3,
        eventId: 1,
        hackerStatus: 'wins',
        xp: 150,
        coins: 15
    }, {
        hackerId: 3,
        eventId: 2,
        hackerStatus: 'rejected',
        xp: 0,
        coins: 0
    }
]

for (let p of participations) {
    dbQueries.addParticipation(p)
        .then(p.hackerId + '-' + p.eventId + ' added')
}
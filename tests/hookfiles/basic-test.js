//
// ABAO hooks file 
// Generated from RAML specification
//   RAML: api.raml
//   Date: 2018-06-15 18:00:00
// <https://github.com/cybertk/abao>
//
const hooks = require('hooks');
const { assert } = require('chai');

//
// Setup/Teardown
//
hooks.beforeAll(function (done) {
  console.log("HI!")
  done();
});

hooks.afterAll(function (done) {
  done();
});

//
// Hooks
//
//-----------------------------------------------------------------------------
hooks.before('POST /login -> 200', function (test, done) {
  // Modify 'test.request' properties here to modify the inbound request
  done();
});

hooks.after('POST /login -> 200', function (test, done) {
  // Assert against 'test.response' properties here to verify expected results
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /login -> 401', function (test, done) {
  done();
});

hooks.after('POST /login -> 401', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /login/telegram -> 200', function (test, done) {
  done();
});

hooks.after('POST /login/telegram -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /login/telegram -> 401', function (test, done) {
  done();
});

hooks.after('POST /login/telegram -> 401', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('GET /hackers -> 200', function (test, done) {
  done();
});

hooks.after('GET /hackers -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /hackers -> 200', function (test, done) {
  done();
});

hooks.after('POST /hackers -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /hackers -> 400', function (test, done) {
  done();
});

hooks.after('POST /hackers -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('GET /hackers/{id} -> 200', function (test, done) {
  done();
});

hooks.after('GET /hackers/{id} -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('GET /hackers/{id} -> 404', function (test, done) {
  done();
});

hooks.after('GET /hackers/{id} -> 404', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('PUT /hackers/me -> 200', function (test, done) {
  done();
});

hooks.after('PUT /hackers/me -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('PUT /hackers/me -> 400', function (test, done) {
  done();
});

hooks.after('PUT /hackers/me -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('GET /skills -> 200', function (test, done) {
  done();
});

hooks.after('GET /skills -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('GET /events -> 200', function (test, done) {
  done();
});

hooks.after('GET /events -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/toggle_is_searchable -> 200', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/toggle_is_searchable -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/toggle_is_searchable -> 400', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/toggle_is_searchable -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/apply -> 200', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/apply -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/apply -> 400', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/apply -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/apply -> 409', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/apply -> 409', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/activate -> 200', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/activate -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/activate -> 400', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/activate -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/finish -> 200', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/finish -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /events/{eventId}/finish -> 400', function (test, done) {
  done();
});

hooks.after('POST /events/{eventId}/finish -> 400', function (test, done) {
  done();
});


hooks.skip('POST /login -> 200');
hooks.skip('POST /login -> 401');
hooks.skip('POST /login/telegram -> 200');
hooks.skip('POST /login/telegram -> 401');
hooks.skip('GET /hackers -> 200');
hooks.skip('POST /hackers -> 200');
hooks.skip('POST /hackers -> 400');
hooks.skip('GET /hackers/{id} -> 200');
hooks.skip('GET /hackers/{id} -> 404');
hooks.skip('PUT /hackers/me -> 200');
hooks.skip('PUT /hackers/me -> 400');
hooks.skip('GET /skills -> 200');
hooks.skip('GET /events -> 200');
hooks.skip('POST /events/{eventId}/toggle_is_searchable -> 200');
hooks.skip('POST /events/{eventId}/toggle_is_searchable -> 400');
hooks.skip('POST /events/{eventId}/apply -> 200');
hooks.skip('POST /events/{eventId}/apply -> 400');
hooks.skip('POST /events/{eventId}/apply -> 409');
hooks.skip('POST /events/{eventId}/activate -> 200');
hooks.skip('POST /events/{eventId}/activate -> 400');
hooks.skip('POST /events/{eventId}/finish -> 200');
hooks.skip('POST /events/{eventId}/finish -> 400');


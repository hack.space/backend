## Project Structure

```
├── admin --contains test html files--
│   └── index.html *telegram login widget usage example*
├── apidoc *documentation of this API*
│   ├── api.raml *raml file - description of end-points*
│   ├── examples
|   |   ├── entities.method.example.json
|   |   └── ...
│   └── schemas
|       ├── entities.method.schema.json
|       └── ...
├── app.js
├── _ci
│   ├── deploy.sh
│   └── seed-db.js## DB structure

https://drive.google.com/file/d/10xZZ-4CImmnMZ2AVFTZBlUlMoX8vY4O8/view?usp=sharing

├── src
│   ├── controllers
|   |   ├── entity.controller.json
|   |   └── ...
│   ├── ESBs
|   |   ├── entity.esb.json
|   |   └── ...
│   ├── pg-util.js
│   ├── plugins
|   |   └── authN.js
│   ├── routes
|   |   ├── entities.router.json
|   |   └── ...
│   └── sql
|       └── init.sql
├── .Dockerfile
├── .gitignore
├── .http
├── app.js
├── package.json
├── README.md
└── yarn.lock
```

- `/apidoc` - general folder of api design
- `/apidoc/api.raml` - main documentation for end-points. If you face some problems with it, ask Roman
- `/apidoc/examples` - contains examples of body in responses and requests for end-points provided in raml doc
- `/apidoc/schemas` - contains schemas of JSON objects which should be sent or received in requests.

## Authorization

- /login/ - basic login. If credentials are valid -> you get jwt, set it in `authorization` header and send it with each request (however it is not needed in some requests). 
- /login/telegram/ - login using telegram login widget. Look for example in `/admin/index.html`

## Registration

- Use POST /api/hackers/ request - you'll get jwt after successfull creating of a user.

## Participation flow

**Participation statuses:**
- `Open` - when a user not registered yet and hackathon didn't started.
- `Applied`
- `Confirmed`
- `Declined`
- `Activated`
- `Participated`
- `Won`

## Usefull docs
- [Email verification flow](https://www.notion.so/hackspace/Email-verification-for-participants-1f27fc43979a430996e0936e14d1fd16)


# Confirmed

Confirmer worker is used for user data verification, such phone and e-mail.

There are 3 types of checking:

- Email verification: ev/:id
- Password restore: pr/:id
- New user's application: nu/:id

Each checking row is stored in Redis, so one group should contain only one row for security reasons. There are cannot be two email-verification row for a one user.
require('dotenv').config() // source .env

const express = require('express');
const glob = require('glob');
const { join } = require('path');
const bodyParser = require('body-parser');
const helmet = require('helmet');

const app = express();
app.use(helmet());

const ENABLE_CORS = process.env.ENABLE_CORS === 'true';

app.use(bodyParser.json());

app.use(function(req,res,next){
  if (req.method === 'OPTIONS')
    res.send(200);
  next();
});

// Add headers
app.use(function (req, res, next) {
  if (ENABLE_CORS){ 
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  };
  next();
});

// Plug-in routes.
glob('src/routes/*.js', (err, pathes) => {
  for (let path of pathes)
    app.use(`/api/${/\/(\w+)\./.exec(path)[1]}`, require(`./${path}`));
});

app.get('/api/docdocdoc', function (req, res) {
  res.sendFile(join(__dirname, "apidoc", "api.html"));
});

const PORT = process.env.PORT || 8081;

app.listen(PORT, err => {
  if (!err)
    console.log('Service is listening on port ' + PORT)
});